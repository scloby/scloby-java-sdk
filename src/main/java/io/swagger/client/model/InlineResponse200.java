/*
 * Scloby Api
 * **Introduction**  **The authentication token is issued via Oauth2 standard ( https://oauth.net)**    You can apply for a Scloby test account on our developer portal **https://developer.scloby.com**   Here is your **ClientId** and **ClientSecret** for tests:    **client_id**: SclobyApiDocs    **client_secret**: DdyPNPvfPOGa0Izjct0C   **All data is JSON format, and the Content-Type header of POST/PUT request must be set to application/json.**  **Search Parameters**  You can perform a search in a collection using all first level properties in combination with some parameter, or specifying a value:    property = VALUE if you are searching for all items with a specific property value property_since = MIN_VALUE   if you are searching for all items with property value greater (or equal) than MIN_VALUE property_max   = MAX_VALUE   if you are searching for all items with property value smaller (or equal)than MAX_VALUE property_like  = LIKE_VALUE   if you are searching for a substring content You can use property_since and property_max in order to simulate **between** operator   If you are using *equal* operator, all other operators for that parameter *will be ingnored*   If you are using _max or _since operator in strings, the alphabetical ordering will be used
 *
 * OpenAPI spec version: 2.0.0
 * Contact: dev@scloby.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 * InlineResponse200
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2020-11-03T17:06:12.537Z[GMT]")
public class InlineResponse200 {
  @SerializedName("weekdays_period")
  private List<String> weekdaysPeriod = null;

  @SerializedName("id")
  private Integer id = null;

  @SerializedName("name")
  private String name = null;

  @SerializedName("start_time")
  private String startTime = null;

  @SerializedName("end_time")
  private String endTime = null;

  @SerializedName("start_period")
  private String startPeriod = null;

  @SerializedName("end_period")
  private String endPeriod = null;

  @SerializedName("default_duration")
  private Integer defaultDuration = null;

  @SerializedName("default_pricelist")
  private Integer defaultPricelist = null;

  @SerializedName("instore_seats_limit")
  private Integer instoreSeatsLimit = null;

  @SerializedName("online_seats_limit")
  private Integer onlineSeatsLimit = null;

  @SerializedName("room_restrictions")
  private List<String> roomRestrictions = null;

  public InlineResponse200 weekdaysPeriod(List<String> weekdaysPeriod) {
    this.weekdaysPeriod = weekdaysPeriod;
    return this;
  }

  public InlineResponse200 addWeekdaysPeriodItem(String weekdaysPeriodItem) {
    if (this.weekdaysPeriod == null) {
      this.weekdaysPeriod = new ArrayList<String>();
    }
    this.weekdaysPeriod.add(weekdaysPeriodItem);
    return this;
  }

   /**
   * Get weekdaysPeriod
   * @return weekdaysPeriod
  **/
  @Schema(description = "")
  public List<String> getWeekdaysPeriod() {
    return weekdaysPeriod;
  }

  public void setWeekdaysPeriod(List<String> weekdaysPeriod) {
    this.weekdaysPeriod = weekdaysPeriod;
  }

  public InlineResponse200 id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @Schema(example = "2", description = "")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public InlineResponse200 name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @Schema(example = "Cena", description = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public InlineResponse200 startTime(String startTime) {
    this.startTime = startTime;
    return this;
  }

   /**
   * Get startTime
   * @return startTime
  **/
  @Schema(example = "68400", description = "")
  public String getStartTime() {
    return startTime;
  }

  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }

  public InlineResponse200 endTime(String endTime) {
    this.endTime = endTime;
    return this;
  }

   /**
   * Get endTime
   * @return endTime
  **/
  @Schema(example = "86400", description = "")
  public String getEndTime() {
    return endTime;
  }

  public void setEndTime(String endTime) {
    this.endTime = endTime;
  }

  public InlineResponse200 startPeriod(String startPeriod) {
    this.startPeriod = startPeriod;
    return this;
  }

   /**
   * Get startPeriod
   * @return startPeriod
  **/
  @Schema(example = "2020-01-01T00:00:00.000Z", description = "")
  public String getStartPeriod() {
    return startPeriod;
  }

  public void setStartPeriod(String startPeriod) {
    this.startPeriod = startPeriod;
  }

  public InlineResponse200 endPeriod(String endPeriod) {
    this.endPeriod = endPeriod;
    return this;
  }

   /**
   * Get endPeriod
   * @return endPeriod
  **/
  @Schema(example = "2020-12-31T00:00:00.000Z", description = "")
  public String getEndPeriod() {
    return endPeriod;
  }

  public void setEndPeriod(String endPeriod) {
    this.endPeriod = endPeriod;
  }

  public InlineResponse200 defaultDuration(Integer defaultDuration) {
    this.defaultDuration = defaultDuration;
    return this;
  }

   /**
   * Get defaultDuration
   * @return defaultDuration
  **/
  @Schema(example = "60", description = "")
  public Integer getDefaultDuration() {
    return defaultDuration;
  }

  public void setDefaultDuration(Integer defaultDuration) {
    this.defaultDuration = defaultDuration;
  }

  public InlineResponse200 defaultPricelist(Integer defaultPricelist) {
    this.defaultPricelist = defaultPricelist;
    return this;
  }

   /**
   * Get defaultPricelist
   * @return defaultPricelist
  **/
  @Schema(example = "0", description = "")
  public Integer getDefaultPricelist() {
    return defaultPricelist;
  }

  public void setDefaultPricelist(Integer defaultPricelist) {
    this.defaultPricelist = defaultPricelist;
  }

  public InlineResponse200 instoreSeatsLimit(Integer instoreSeatsLimit) {
    this.instoreSeatsLimit = instoreSeatsLimit;
    return this;
  }

   /**
   * Get instoreSeatsLimit
   * @return instoreSeatsLimit
  **/
  @Schema(example = "0", description = "")
  public Integer getInstoreSeatsLimit() {
    return instoreSeatsLimit;
  }

  public void setInstoreSeatsLimit(Integer instoreSeatsLimit) {
    this.instoreSeatsLimit = instoreSeatsLimit;
  }

  public InlineResponse200 onlineSeatsLimit(Integer onlineSeatsLimit) {
    this.onlineSeatsLimit = onlineSeatsLimit;
    return this;
  }

   /**
   * Get onlineSeatsLimit
   * @return onlineSeatsLimit
  **/
  @Schema(example = "0", description = "")
  public Integer getOnlineSeatsLimit() {
    return onlineSeatsLimit;
  }

  public void setOnlineSeatsLimit(Integer onlineSeatsLimit) {
    this.onlineSeatsLimit = onlineSeatsLimit;
  }

  public InlineResponse200 roomRestrictions(List<String> roomRestrictions) {
    this.roomRestrictions = roomRestrictions;
    return this;
  }

  public InlineResponse200 addRoomRestrictionsItem(String roomRestrictionsItem) {
    if (this.roomRestrictions == null) {
      this.roomRestrictions = new ArrayList<String>();
    }
    this.roomRestrictions.add(roomRestrictionsItem);
    return this;
  }

   /**
   * Get roomRestrictions
   * @return roomRestrictions
  **/
  @Schema(description = "")
  public List<String> getRoomRestrictions() {
    return roomRestrictions;
  }

  public void setRoomRestrictions(List<String> roomRestrictions) {
    this.roomRestrictions = roomRestrictions;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InlineResponse200 inlineResponse200 = (InlineResponse200) o;
    return Objects.equals(this.weekdaysPeriod, inlineResponse200.weekdaysPeriod) &&
        Objects.equals(this.id, inlineResponse200.id) &&
        Objects.equals(this.name, inlineResponse200.name) &&
        Objects.equals(this.startTime, inlineResponse200.startTime) &&
        Objects.equals(this.endTime, inlineResponse200.endTime) &&
        Objects.equals(this.startPeriod, inlineResponse200.startPeriod) &&
        Objects.equals(this.endPeriod, inlineResponse200.endPeriod) &&
        Objects.equals(this.defaultDuration, inlineResponse200.defaultDuration) &&
        Objects.equals(this.defaultPricelist, inlineResponse200.defaultPricelist) &&
        Objects.equals(this.instoreSeatsLimit, inlineResponse200.instoreSeatsLimit) &&
        Objects.equals(this.onlineSeatsLimit, inlineResponse200.onlineSeatsLimit) &&
        Objects.equals(this.roomRestrictions, inlineResponse200.roomRestrictions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(weekdaysPeriod, id, name, startTime, endTime, startPeriod, endPeriod, defaultDuration, defaultPricelist, instoreSeatsLimit, onlineSeatsLimit, roomRestrictions);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InlineResponse200 {\n");
    
    sb.append("    weekdaysPeriod: ").append(toIndentedString(weekdaysPeriod)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    startTime: ").append(toIndentedString(startTime)).append("\n");
    sb.append("    endTime: ").append(toIndentedString(endTime)).append("\n");
    sb.append("    startPeriod: ").append(toIndentedString(startPeriod)).append("\n");
    sb.append("    endPeriod: ").append(toIndentedString(endPeriod)).append("\n");
    sb.append("    defaultDuration: ").append(toIndentedString(defaultDuration)).append("\n");
    sb.append("    defaultPricelist: ").append(toIndentedString(defaultPricelist)).append("\n");
    sb.append("    instoreSeatsLimit: ").append(toIndentedString(instoreSeatsLimit)).append("\n");
    sb.append("    onlineSeatsLimit: ").append(toIndentedString(onlineSeatsLimit)).append("\n");
    sb.append("    roomRestrictions: ").append(toIndentedString(roomRestrictions)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
