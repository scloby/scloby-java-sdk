/*
 * Scloby Api
 * **Introduction**  **The authentication token is issued via Oauth2 standard ( https://oauth.net)**    You can apply for a Scloby test account on our developer portal **https://developer.scloby.com**   Here is your **ClientId** and **ClientSecret** for tests:    **client_id**: SclobyApiDocs    **client_secret**: DdyPNPvfPOGa0Izjct0C   **All data is JSON format, and the Content-Type header of POST/PUT request must be set to application/json.**  **Search Parameters**  You can perform a search in a collection using all first level properties in combination with some parameter, or specifying a value:    property = VALUE if you are searching for all items with a specific property value property_since = MIN_VALUE   if you are searching for all items with property value greater (or equal) than MIN_VALUE property_max   = MAX_VALUE   if you are searching for all items with property value smaller (or equal)than MAX_VALUE property_like  = LIKE_VALUE   if you are searching for a substring content You can use property_since and property_max in order to simulate **between** operator   If you are using *equal* operator, all other operators for that parameter *will be ingnored*   If you are using _max or _since operator in strings, the alphabetical ordering will be used
 *
 * OpenAPI spec version: 2.0.0
 * Contact: dev@scloby.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
import org.threeten.bp.OffsetDateTime;
/**
 * Supplier
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2020-11-03T17:06:12.537Z[GMT]")
public class Supplier {
  @SerializedName("id")
  private Integer id = null;

  @SerializedName("name")
  private String name = null;

  @SerializedName("thumbnail")
  private String thumbnail = null;

  @SerializedName("address_street")
  private String addressStreet = null;

  @SerializedName("address_number")
  private String addressNumber = null;

  @SerializedName("address_city")
  private String addressCity = null;

  @SerializedName("address_zip")
  private String addressZip = null;

  @SerializedName("address_prov")
  private String addressProv = null;

  @SerializedName("phone1")
  private String phone1 = null;

  @SerializedName("phone2")
  private String phone2 = null;

  @SerializedName("fax")
  private String fax = null;

  @SerializedName("email1")
  private String email1 = null;

  @SerializedName("email2")
  private String email2 = null;

  @SerializedName("email_pec")
  private String emailPec = null;

  @SerializedName("website")
  private String website = null;

  @SerializedName("vat_code")
  private String vatCode = null;

  @SerializedName("notes")
  private String notes = null;

  @SerializedName("deleted_at")
  private OffsetDateTime deletedAt = null;

  @SerializedName("createdby_id")
  private Integer createdbyId = null;

  @SerializedName("updatedby_id")
  private Integer updatedbyId = null;

  @SerializedName("deletedby_id")
  private Integer deletedbyId = null;

   /**
   * Get id
   * @return id
  **/
  @Schema(description = "")
  public Integer getId() {
    return id;
  }

  public Supplier name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @Schema(required = true, description = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Supplier thumbnail(String thumbnail) {
    this.thumbnail = thumbnail;
    return this;
  }

   /**
   * Get thumbnail
   * @return thumbnail
  **/
  @Schema(description = "")
  public String getThumbnail() {
    return thumbnail;
  }

  public void setThumbnail(String thumbnail) {
    this.thumbnail = thumbnail;
  }

  public Supplier addressStreet(String addressStreet) {
    this.addressStreet = addressStreet;
    return this;
  }

   /**
   * Get addressStreet
   * @return addressStreet
  **/
  @Schema(description = "")
  public String getAddressStreet() {
    return addressStreet;
  }

  public void setAddressStreet(String addressStreet) {
    this.addressStreet = addressStreet;
  }

  public Supplier addressNumber(String addressNumber) {
    this.addressNumber = addressNumber;
    return this;
  }

   /**
   * Get addressNumber
   * @return addressNumber
  **/
  @Schema(description = "")
  public String getAddressNumber() {
    return addressNumber;
  }

  public void setAddressNumber(String addressNumber) {
    this.addressNumber = addressNumber;
  }

  public Supplier addressCity(String addressCity) {
    this.addressCity = addressCity;
    return this;
  }

   /**
   * Get addressCity
   * @return addressCity
  **/
  @Schema(description = "")
  public String getAddressCity() {
    return addressCity;
  }

  public void setAddressCity(String addressCity) {
    this.addressCity = addressCity;
  }

  public Supplier addressZip(String addressZip) {
    this.addressZip = addressZip;
    return this;
  }

   /**
   * Get addressZip
   * @return addressZip
  **/
  @Schema(description = "")
  public String getAddressZip() {
    return addressZip;
  }

  public void setAddressZip(String addressZip) {
    this.addressZip = addressZip;
  }

  public Supplier addressProv(String addressProv) {
    this.addressProv = addressProv;
    return this;
  }

   /**
   * Get addressProv
   * @return addressProv
  **/
  @Schema(description = "")
  public String getAddressProv() {
    return addressProv;
  }

  public void setAddressProv(String addressProv) {
    this.addressProv = addressProv;
  }

  public Supplier phone1(String phone1) {
    this.phone1 = phone1;
    return this;
  }

   /**
   * Get phone1
   * @return phone1
  **/
  @Schema(description = "")
  public String getPhone1() {
    return phone1;
  }

  public void setPhone1(String phone1) {
    this.phone1 = phone1;
  }

  public Supplier phone2(String phone2) {
    this.phone2 = phone2;
    return this;
  }

   /**
   * Get phone2
   * @return phone2
  **/
  @Schema(description = "")
  public String getPhone2() {
    return phone2;
  }

  public void setPhone2(String phone2) {
    this.phone2 = phone2;
  }

  public Supplier fax(String fax) {
    this.fax = fax;
    return this;
  }

   /**
   * Get fax
   * @return fax
  **/
  @Schema(description = "")
  public String getFax() {
    return fax;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }

  public Supplier email1(String email1) {
    this.email1 = email1;
    return this;
  }

   /**
   * Get email1
   * @return email1
  **/
  @Schema(description = "")
  public String getEmail1() {
    return email1;
  }

  public void setEmail1(String email1) {
    this.email1 = email1;
  }

  public Supplier email2(String email2) {
    this.email2 = email2;
    return this;
  }

   /**
   * Get email2
   * @return email2
  **/
  @Schema(description = "")
  public String getEmail2() {
    return email2;
  }

  public void setEmail2(String email2) {
    this.email2 = email2;
  }

  public Supplier emailPec(String emailPec) {
    this.emailPec = emailPec;
    return this;
  }

   /**
   * Get emailPec
   * @return emailPec
  **/
  @Schema(description = "")
  public String getEmailPec() {
    return emailPec;
  }

  public void setEmailPec(String emailPec) {
    this.emailPec = emailPec;
  }

  public Supplier website(String website) {
    this.website = website;
    return this;
  }

   /**
   * Get website
   * @return website
  **/
  @Schema(description = "")
  public String getWebsite() {
    return website;
  }

  public void setWebsite(String website) {
    this.website = website;
  }

  public Supplier vatCode(String vatCode) {
    this.vatCode = vatCode;
    return this;
  }

   /**
   * Get vatCode
   * @return vatCode
  **/
  @Schema(required = true, description = "")
  public String getVatCode() {
    return vatCode;
  }

  public void setVatCode(String vatCode) {
    this.vatCode = vatCode;
  }

  public Supplier notes(String notes) {
    this.notes = notes;
    return this;
  }

   /**
   * Get notes
   * @return notes
  **/
  @Schema(description = "")
  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

   /**
   * Get deletedAt
   * @return deletedAt
  **/
  @Schema(description = "")
  public OffsetDateTime getDeletedAt() {
    return deletedAt;
  }

  public Supplier createdbyId(Integer createdbyId) {
    this.createdbyId = createdbyId;
    return this;
  }

   /**
   * Get createdbyId
   * @return createdbyId
  **/
  @Schema(description = "")
  public Integer getCreatedbyId() {
    return createdbyId;
  }

  public void setCreatedbyId(Integer createdbyId) {
    this.createdbyId = createdbyId;
  }

  public Supplier updatedbyId(Integer updatedbyId) {
    this.updatedbyId = updatedbyId;
    return this;
  }

   /**
   * Get updatedbyId
   * @return updatedbyId
  **/
  @Schema(description = "")
  public Integer getUpdatedbyId() {
    return updatedbyId;
  }

  public void setUpdatedbyId(Integer updatedbyId) {
    this.updatedbyId = updatedbyId;
  }

  public Supplier deletedbyId(Integer deletedbyId) {
    this.deletedbyId = deletedbyId;
    return this;
  }

   /**
   * Get deletedbyId
   * @return deletedbyId
  **/
  @Schema(description = "")
  public Integer getDeletedbyId() {
    return deletedbyId;
  }

  public void setDeletedbyId(Integer deletedbyId) {
    this.deletedbyId = deletedbyId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Supplier supplier = (Supplier) o;
    return Objects.equals(this.id, supplier.id) &&
        Objects.equals(this.name, supplier.name) &&
        Objects.equals(this.thumbnail, supplier.thumbnail) &&
        Objects.equals(this.addressStreet, supplier.addressStreet) &&
        Objects.equals(this.addressNumber, supplier.addressNumber) &&
        Objects.equals(this.addressCity, supplier.addressCity) &&
        Objects.equals(this.addressZip, supplier.addressZip) &&
        Objects.equals(this.addressProv, supplier.addressProv) &&
        Objects.equals(this.phone1, supplier.phone1) &&
        Objects.equals(this.phone2, supplier.phone2) &&
        Objects.equals(this.fax, supplier.fax) &&
        Objects.equals(this.email1, supplier.email1) &&
        Objects.equals(this.email2, supplier.email2) &&
        Objects.equals(this.emailPec, supplier.emailPec) &&
        Objects.equals(this.website, supplier.website) &&
        Objects.equals(this.vatCode, supplier.vatCode) &&
        Objects.equals(this.notes, supplier.notes) &&
        Objects.equals(this.deletedAt, supplier.deletedAt) &&
        Objects.equals(this.createdbyId, supplier.createdbyId) &&
        Objects.equals(this.updatedbyId, supplier.updatedbyId) &&
        Objects.equals(this.deletedbyId, supplier.deletedbyId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, thumbnail, addressStreet, addressNumber, addressCity, addressZip, addressProv, phone1, phone2, fax, email1, email2, emailPec, website, vatCode, notes, deletedAt, createdbyId, updatedbyId, deletedbyId);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Supplier {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    thumbnail: ").append(toIndentedString(thumbnail)).append("\n");
    sb.append("    addressStreet: ").append(toIndentedString(addressStreet)).append("\n");
    sb.append("    addressNumber: ").append(toIndentedString(addressNumber)).append("\n");
    sb.append("    addressCity: ").append(toIndentedString(addressCity)).append("\n");
    sb.append("    addressZip: ").append(toIndentedString(addressZip)).append("\n");
    sb.append("    addressProv: ").append(toIndentedString(addressProv)).append("\n");
    sb.append("    phone1: ").append(toIndentedString(phone1)).append("\n");
    sb.append("    phone2: ").append(toIndentedString(phone2)).append("\n");
    sb.append("    fax: ").append(toIndentedString(fax)).append("\n");
    sb.append("    email1: ").append(toIndentedString(email1)).append("\n");
    sb.append("    email2: ").append(toIndentedString(email2)).append("\n");
    sb.append("    emailPec: ").append(toIndentedString(emailPec)).append("\n");
    sb.append("    website: ").append(toIndentedString(website)).append("\n");
    sb.append("    vatCode: ").append(toIndentedString(vatCode)).append("\n");
    sb.append("    notes: ").append(toIndentedString(notes)).append("\n");
    sb.append("    deletedAt: ").append(toIndentedString(deletedAt)).append("\n");
    sb.append("    createdbyId: ").append(toIndentedString(createdbyId)).append("\n");
    sb.append("    updatedbyId: ").append(toIndentedString(updatedbyId)).append("\n");
    sb.append("    deletedbyId: ").append(toIndentedString(deletedbyId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
