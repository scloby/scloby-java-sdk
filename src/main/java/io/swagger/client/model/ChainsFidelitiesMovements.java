/*
 * Scloby Api
 * **Introduction**  **The authentication token is issued via Oauth2 standard ( https://oauth.net)**    You can apply for a Scloby test account on our developer portal **https://developer.scloby.com**   Here is your **ClientId** and **ClientSecret** for tests:    **client_id**: SclobyApiDocs    **client_secret**: DdyPNPvfPOGa0Izjct0C   **All data is JSON format, and the Content-Type header of POST/PUT request must be set to application/json.**  **Search Parameters**  You can perform a search in a collection using all first level properties in combination with some parameter, or specifying a value:    property = VALUE if you are searching for all items with a specific property value property_since = MIN_VALUE   if you are searching for all items with property value greater (or equal) than MIN_VALUE property_max   = MAX_VALUE   if you are searching for all items with property value smaller (or equal)than MAX_VALUE property_like  = LIKE_VALUE   if you are searching for a substring content You can use property_since and property_max in order to simulate **between** operator   If you are using *equal* operator, all other operators for that parameter *will be ingnored*   If you are using _max or _since operator in strings, the alphabetical ordering will be used
 *
 * OpenAPI spec version: 2.0.0
 * Contact: dev@scloby.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
import org.threeten.bp.OffsetDateTime;
/**
 * ChainsFidelitiesMovements
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2020-11-03T17:06:12.537Z[GMT]")
public class ChainsFidelitiesMovements {
  @SerializedName("id")
  private Integer id = null;

  @SerializedName("date")
  private OffsetDateTime date = null;

  @SerializedName("db_name")
  private String dbName = null;

  @SerializedName("shop_name")
  private String shopName = null;

  @SerializedName("fidelity")
  private String fidelity = null;

  @SerializedName("campaign_id")
  private Integer campaignId = null;

  @SerializedName("campaign_name")
  private String campaignName = null;

  @SerializedName("rule_id")
  private Integer ruleId = null;

  @SerializedName("rule_name")
  private String ruleName = null;

  @SerializedName("prize_id")
  private Integer prizeId = null;

  @SerializedName("sale_id")
  private Integer saleId = null;

  @SerializedName("sale_name")
  private String saleName = null;

  @SerializedName("item_sku")
  private String itemSku = null;

  @SerializedName("item_name")
  private String itemName = null;

  @SerializedName("points")
  private Integer points = null;

  @SerializedName("notes")
  private String notes = null;

   /**
   * Get id
   * @return id
  **/
  @Schema(description = "")
  public Integer getId() {
    return id;
  }

  public ChainsFidelitiesMovements date(OffsetDateTime date) {
    this.date = date;
    return this;
  }

   /**
   * Get date
   * @return date
  **/
  @Schema(required = true, description = "")
  public OffsetDateTime getDate() {
    return date;
  }

  public void setDate(OffsetDateTime date) {
    this.date = date;
  }

  public ChainsFidelitiesMovements dbName(String dbName) {
    this.dbName = dbName;
    return this;
  }

   /**
   * Get dbName
   * @return dbName
  **/
  @Schema(required = true, description = "")
  public String getDbName() {
    return dbName;
  }

  public void setDbName(String dbName) {
    this.dbName = dbName;
  }

  public ChainsFidelitiesMovements shopName(String shopName) {
    this.shopName = shopName;
    return this;
  }

   /**
   * Get shopName
   * @return shopName
  **/
  @Schema(description = "")
  public String getShopName() {
    return shopName;
  }

  public void setShopName(String shopName) {
    this.shopName = shopName;
  }

  public ChainsFidelitiesMovements fidelity(String fidelity) {
    this.fidelity = fidelity;
    return this;
  }

   /**
   * Get fidelity
   * @return fidelity
  **/
  @Schema(required = true, description = "")
  public String getFidelity() {
    return fidelity;
  }

  public void setFidelity(String fidelity) {
    this.fidelity = fidelity;
  }

  public ChainsFidelitiesMovements campaignId(Integer campaignId) {
    this.campaignId = campaignId;
    return this;
  }

   /**
   * Get campaignId
   * @return campaignId
  **/
  @Schema(required = true, description = "")
  public Integer getCampaignId() {
    return campaignId;
  }

  public void setCampaignId(Integer campaignId) {
    this.campaignId = campaignId;
  }

  public ChainsFidelitiesMovements campaignName(String campaignName) {
    this.campaignName = campaignName;
    return this;
  }

   /**
   * Get campaignName
   * @return campaignName
  **/
  @Schema(required = true, description = "")
  public String getCampaignName() {
    return campaignName;
  }

  public void setCampaignName(String campaignName) {
    this.campaignName = campaignName;
  }

  public ChainsFidelitiesMovements ruleId(Integer ruleId) {
    this.ruleId = ruleId;
    return this;
  }

   /**
   * Get ruleId
   * @return ruleId
  **/
  @Schema(description = "")
  public Integer getRuleId() {
    return ruleId;
  }

  public void setRuleId(Integer ruleId) {
    this.ruleId = ruleId;
  }

  public ChainsFidelitiesMovements ruleName(String ruleName) {
    this.ruleName = ruleName;
    return this;
  }

   /**
   * Get ruleName
   * @return ruleName
  **/
  @Schema(description = "")
  public String getRuleName() {
    return ruleName;
  }

  public void setRuleName(String ruleName) {
    this.ruleName = ruleName;
  }

  public ChainsFidelitiesMovements prizeId(Integer prizeId) {
    this.prizeId = prizeId;
    return this;
  }

   /**
   * Get prizeId
   * @return prizeId
  **/
  @Schema(description = "")
  public Integer getPrizeId() {
    return prizeId;
  }

  public void setPrizeId(Integer prizeId) {
    this.prizeId = prizeId;
  }

  public ChainsFidelitiesMovements saleId(Integer saleId) {
    this.saleId = saleId;
    return this;
  }

   /**
   * Get saleId
   * @return saleId
  **/
  @Schema(description = "")
  public Integer getSaleId() {
    return saleId;
  }

  public void setSaleId(Integer saleId) {
    this.saleId = saleId;
  }

  public ChainsFidelitiesMovements saleName(String saleName) {
    this.saleName = saleName;
    return this;
  }

   /**
   * Get saleName
   * @return saleName
  **/
  @Schema(description = "")
  public String getSaleName() {
    return saleName;
  }

  public void setSaleName(String saleName) {
    this.saleName = saleName;
  }

  public ChainsFidelitiesMovements itemSku(String itemSku) {
    this.itemSku = itemSku;
    return this;
  }

   /**
   * Get itemSku
   * @return itemSku
  **/
  @Schema(description = "")
  public String getItemSku() {
    return itemSku;
  }

  public void setItemSku(String itemSku) {
    this.itemSku = itemSku;
  }

  public ChainsFidelitiesMovements itemName(String itemName) {
    this.itemName = itemName;
    return this;
  }

   /**
   * Get itemName
   * @return itemName
  **/
  @Schema(description = "")
  public String getItemName() {
    return itemName;
  }

  public void setItemName(String itemName) {
    this.itemName = itemName;
  }

  public ChainsFidelitiesMovements points(Integer points) {
    this.points = points;
    return this;
  }

   /**
   * Get points
   * @return points
  **/
  @Schema(required = true, description = "")
  public Integer getPoints() {
    return points;
  }

  public void setPoints(Integer points) {
    this.points = points;
  }

  public ChainsFidelitiesMovements notes(String notes) {
    this.notes = notes;
    return this;
  }

   /**
   * Get notes
   * @return notes
  **/
  @Schema(description = "")
  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ChainsFidelitiesMovements chainsFidelitiesMovements = (ChainsFidelitiesMovements) o;
    return Objects.equals(this.id, chainsFidelitiesMovements.id) &&
        Objects.equals(this.date, chainsFidelitiesMovements.date) &&
        Objects.equals(this.dbName, chainsFidelitiesMovements.dbName) &&
        Objects.equals(this.shopName, chainsFidelitiesMovements.shopName) &&
        Objects.equals(this.fidelity, chainsFidelitiesMovements.fidelity) &&
        Objects.equals(this.campaignId, chainsFidelitiesMovements.campaignId) &&
        Objects.equals(this.campaignName, chainsFidelitiesMovements.campaignName) &&
        Objects.equals(this.ruleId, chainsFidelitiesMovements.ruleId) &&
        Objects.equals(this.ruleName, chainsFidelitiesMovements.ruleName) &&
        Objects.equals(this.prizeId, chainsFidelitiesMovements.prizeId) &&
        Objects.equals(this.saleId, chainsFidelitiesMovements.saleId) &&
        Objects.equals(this.saleName, chainsFidelitiesMovements.saleName) &&
        Objects.equals(this.itemSku, chainsFidelitiesMovements.itemSku) &&
        Objects.equals(this.itemName, chainsFidelitiesMovements.itemName) &&
        Objects.equals(this.points, chainsFidelitiesMovements.points) &&
        Objects.equals(this.notes, chainsFidelitiesMovements.notes);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, date, dbName, shopName, fidelity, campaignId, campaignName, ruleId, ruleName, prizeId, saleId, saleName, itemSku, itemName, points, notes);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ChainsFidelitiesMovements {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("    dbName: ").append(toIndentedString(dbName)).append("\n");
    sb.append("    shopName: ").append(toIndentedString(shopName)).append("\n");
    sb.append("    fidelity: ").append(toIndentedString(fidelity)).append("\n");
    sb.append("    campaignId: ").append(toIndentedString(campaignId)).append("\n");
    sb.append("    campaignName: ").append(toIndentedString(campaignName)).append("\n");
    sb.append("    ruleId: ").append(toIndentedString(ruleId)).append("\n");
    sb.append("    ruleName: ").append(toIndentedString(ruleName)).append("\n");
    sb.append("    prizeId: ").append(toIndentedString(prizeId)).append("\n");
    sb.append("    saleId: ").append(toIndentedString(saleId)).append("\n");
    sb.append("    saleName: ").append(toIndentedString(saleName)).append("\n");
    sb.append("    itemSku: ").append(toIndentedString(itemSku)).append("\n");
    sb.append("    itemName: ").append(toIndentedString(itemName)).append("\n");
    sb.append("    points: ").append(toIndentedString(points)).append("\n");
    sb.append("    notes: ").append(toIndentedString(notes)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
