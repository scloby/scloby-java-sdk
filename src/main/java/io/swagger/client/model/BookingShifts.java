/*
 * Scloby Api
 * **Introduction**  **The authentication token is issued via Oauth2 standard ( https://oauth.net)**    You can apply for a Scloby test account on our developer portal **https://developer.scloby.com**   Here is your **ClientId** and **ClientSecret** for tests:    **client_id**: SclobyApiDocs    **client_secret**: DdyPNPvfPOGa0Izjct0C   **All data is JSON format, and the Content-Type header of POST/PUT request must be set to application/json.**  **Search Parameters**  You can perform a search in a collection using all first level properties in combination with some parameter, or specifying a value:    property = VALUE if you are searching for all items with a specific property value property_since = MIN_VALUE   if you are searching for all items with property value greater (or equal) than MIN_VALUE property_max   = MAX_VALUE   if you are searching for all items with property value smaller (or equal)than MAX_VALUE property_like  = LIKE_VALUE   if you are searching for a substring content You can use property_since and property_max in order to simulate **between** operator   If you are using *equal* operator, all other operators for that parameter *will be ingnored*   If you are using _max or _since operator in strings, the alphabetical ordering will be used
 *
 * OpenAPI spec version: 2.0.0
 * Contact: dev@scloby.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.client.model.BookingtableSchema;
import io.swagger.client.model.BookingtagSchema;
import io.swagger.client.model.CustomerSchema1;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.threeten.bp.OffsetDateTime;
/**
 * BookingShifts
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2020-11-03T17:06:12.537Z[GMT]")
public class BookingShifts {
  @SerializedName("id")
  private Integer id = null;

  @SerializedName("shift_id")
  private Integer shiftId = null;

  @SerializedName("customer_id")
  private Integer customerId = null;

  @SerializedName("source")
  private String source = null;

  @SerializedName("booked_for")
  private OffsetDateTime bookedFor = null;

  @SerializedName("duration")
  private Integer duration = null;

  @SerializedName("status")
  private String status = null;

  @SerializedName("people")
  private Integer people = null;

  @SerializedName("notes")
  private String notes = null;

  @SerializedName("deleted_at")
  private OffsetDateTime deletedAt = null;

  @SerializedName("createdby_id")
  private Integer createdbyId = null;

  @SerializedName("updatedby_id")
  private Integer updatedbyId = null;

  @SerializedName("deletedby_id")
  private Integer deletedbyId = null;

  @SerializedName("tags")
  private List<BookingtagSchema> tags = null;

  @SerializedName("tables")
  private List<BookingtableSchema> tables = null;

  @SerializedName("customer")
  private CustomerSchema1 customer = null;

   /**
   * Get id
   * @return id
  **/
  @Schema(description = "")
  public Integer getId() {
    return id;
  }

  public BookingShifts shiftId(Integer shiftId) {
    this.shiftId = shiftId;
    return this;
  }

   /**
   * Get shiftId
   * @return shiftId
  **/
  @Schema(description = "")
  public Integer getShiftId() {
    return shiftId;
  }

  public void setShiftId(Integer shiftId) {
    this.shiftId = shiftId;
  }

  public BookingShifts customerId(Integer customerId) {
    this.customerId = customerId;
    return this;
  }

   /**
   * Get customerId
   * @return customerId
  **/
  @Schema(required = true, description = "")
  public Integer getCustomerId() {
    return customerId;
  }

  public void setCustomerId(Integer customerId) {
    this.customerId = customerId;
  }

  public BookingShifts source(String source) {
    this.source = source;
    return this;
  }

   /**
   * Get source
   * @return source
  **/
  @Schema(description = "")
  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public BookingShifts bookedFor(OffsetDateTime bookedFor) {
    this.bookedFor = bookedFor;
    return this;
  }

   /**
   * Get bookedFor
   * @return bookedFor
  **/
  @Schema(required = true, description = "")
  public OffsetDateTime getBookedFor() {
    return bookedFor;
  }

  public void setBookedFor(OffsetDateTime bookedFor) {
    this.bookedFor = bookedFor;
  }

  public BookingShifts duration(Integer duration) {
    this.duration = duration;
    return this;
  }

   /**
   * Get duration
   * @return duration
  **/
  @Schema(required = true, description = "")
  public Integer getDuration() {
    return duration;
  }

  public void setDuration(Integer duration) {
    this.duration = duration;
  }

  public BookingShifts status(String status) {
    this.status = status;
    return this;
  }

   /**
   * Get status
   * @return status
  **/
  @Schema(required = true, description = "")
  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public BookingShifts people(Integer people) {
    this.people = people;
    return this;
  }

   /**
   * Get people
   * @return people
  **/
  @Schema(description = "")
  public Integer getPeople() {
    return people;
  }

  public void setPeople(Integer people) {
    this.people = people;
  }

  public BookingShifts notes(String notes) {
    this.notes = notes;
    return this;
  }

   /**
   * Get notes
   * @return notes
  **/
  @Schema(description = "")
  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

   /**
   * Get deletedAt
   * @return deletedAt
  **/
  @Schema(description = "")
  public OffsetDateTime getDeletedAt() {
    return deletedAt;
  }

  public BookingShifts createdbyId(Integer createdbyId) {
    this.createdbyId = createdbyId;
    return this;
  }

   /**
   * Get createdbyId
   * @return createdbyId
  **/
  @Schema(description = "")
  public Integer getCreatedbyId() {
    return createdbyId;
  }

  public void setCreatedbyId(Integer createdbyId) {
    this.createdbyId = createdbyId;
  }

  public BookingShifts updatedbyId(Integer updatedbyId) {
    this.updatedbyId = updatedbyId;
    return this;
  }

   /**
   * Get updatedbyId
   * @return updatedbyId
  **/
  @Schema(description = "")
  public Integer getUpdatedbyId() {
    return updatedbyId;
  }

  public void setUpdatedbyId(Integer updatedbyId) {
    this.updatedbyId = updatedbyId;
  }

  public BookingShifts deletedbyId(Integer deletedbyId) {
    this.deletedbyId = deletedbyId;
    return this;
  }

   /**
   * Get deletedbyId
   * @return deletedbyId
  **/
  @Schema(description = "")
  public Integer getDeletedbyId() {
    return deletedbyId;
  }

  public void setDeletedbyId(Integer deletedbyId) {
    this.deletedbyId = deletedbyId;
  }

  public BookingShifts tags(List<BookingtagSchema> tags) {
    this.tags = tags;
    return this;
  }

  public BookingShifts addTagsItem(BookingtagSchema tagsItem) {
    if (this.tags == null) {
      this.tags = new ArrayList<BookingtagSchema>();
    }
    this.tags.add(tagsItem);
    return this;
  }

   /**
   * Get tags
   * @return tags
  **/
  @Schema(description = "")
  public List<BookingtagSchema> getTags() {
    return tags;
  }

  public void setTags(List<BookingtagSchema> tags) {
    this.tags = tags;
  }

  public BookingShifts tables(List<BookingtableSchema> tables) {
    this.tables = tables;
    return this;
  }

  public BookingShifts addTablesItem(BookingtableSchema tablesItem) {
    if (this.tables == null) {
      this.tables = new ArrayList<BookingtableSchema>();
    }
    this.tables.add(tablesItem);
    return this;
  }

   /**
   * Get tables
   * @return tables
  **/
  @Schema(description = "")
  public List<BookingtableSchema> getTables() {
    return tables;
  }

  public void setTables(List<BookingtableSchema> tables) {
    this.tables = tables;
  }

  public BookingShifts customer(CustomerSchema1 customer) {
    this.customer = customer;
    return this;
  }

   /**
   * Get customer
   * @return customer
  **/
  @Schema(description = "")
  public CustomerSchema1 getCustomer() {
    return customer;
  }

  public void setCustomer(CustomerSchema1 customer) {
    this.customer = customer;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BookingShifts bookingShifts = (BookingShifts) o;
    return Objects.equals(this.id, bookingShifts.id) &&
        Objects.equals(this.shiftId, bookingShifts.shiftId) &&
        Objects.equals(this.customerId, bookingShifts.customerId) &&
        Objects.equals(this.source, bookingShifts.source) &&
        Objects.equals(this.bookedFor, bookingShifts.bookedFor) &&
        Objects.equals(this.duration, bookingShifts.duration) &&
        Objects.equals(this.status, bookingShifts.status) &&
        Objects.equals(this.people, bookingShifts.people) &&
        Objects.equals(this.notes, bookingShifts.notes) &&
        Objects.equals(this.deletedAt, bookingShifts.deletedAt) &&
        Objects.equals(this.createdbyId, bookingShifts.createdbyId) &&
        Objects.equals(this.updatedbyId, bookingShifts.updatedbyId) &&
        Objects.equals(this.deletedbyId, bookingShifts.deletedbyId) &&
        Objects.equals(this.tags, bookingShifts.tags) &&
        Objects.equals(this.tables, bookingShifts.tables) &&
        Objects.equals(this.customer, bookingShifts.customer);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, shiftId, customerId, source, bookedFor, duration, status, people, notes, deletedAt, createdbyId, updatedbyId, deletedbyId, tags, tables, customer);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BookingShifts {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    shiftId: ").append(toIndentedString(shiftId)).append("\n");
    sb.append("    customerId: ").append(toIndentedString(customerId)).append("\n");
    sb.append("    source: ").append(toIndentedString(source)).append("\n");
    sb.append("    bookedFor: ").append(toIndentedString(bookedFor)).append("\n");
    sb.append("    duration: ").append(toIndentedString(duration)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    people: ").append(toIndentedString(people)).append("\n");
    sb.append("    notes: ").append(toIndentedString(notes)).append("\n");
    sb.append("    deletedAt: ").append(toIndentedString(deletedAt)).append("\n");
    sb.append("    createdbyId: ").append(toIndentedString(createdbyId)).append("\n");
    sb.append("    updatedbyId: ").append(toIndentedString(updatedbyId)).append("\n");
    sb.append("    deletedbyId: ").append(toIndentedString(deletedbyId)).append("\n");
    sb.append("    tags: ").append(toIndentedString(tags)).append("\n");
    sb.append("    tables: ").append(toIndentedString(tables)).append("\n");
    sb.append("    customer: ").append(toIndentedString(customer)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
