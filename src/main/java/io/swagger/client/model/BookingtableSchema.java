/*
 * Scloby Api
 * **Introduction**  **The authentication token is issued via Oauth2 standard ( https://oauth.net)**    You can apply for a Scloby test account on our developer portal **https://developer.scloby.com**   Here is your **ClientId** and **ClientSecret** for tests:    **client_id**: SclobyApiDocs    **client_secret**: DdyPNPvfPOGa0Izjct0C   **All data is JSON format, and the Content-Type header of POST/PUT request must be set to application/json.**  **Search Parameters**  You can perform a search in a collection using all first level properties in combination with some parameter, or specifying a value:    property = VALUE if you are searching for all items with a specific property value property_since = MIN_VALUE   if you are searching for all items with property value greater (or equal) than MIN_VALUE property_max   = MAX_VALUE   if you are searching for all items with property value smaller (or equal)than MAX_VALUE property_like  = LIKE_VALUE   if you are searching for a substring content You can use property_since and property_max in order to simulate **between** operator   If you are using *equal* operator, all other operators for that parameter *will be ingnored*   If you are using _max or _since operator in strings, the alphabetical ordering will be used
 *
 * OpenAPI spec version: 2.0.0
 * Contact: dev@scloby.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
/**
 * BookingtableSchema
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2020-11-03T17:06:12.537Z[GMT]")
public class BookingtableSchema {
  @SerializedName("id")
  private Integer id = null;

  @SerializedName("booking_id")
  private Integer bookingId = null;

  @SerializedName("table_id")
  private String tableId = null;

   /**
   * Get id
   * @return id
  **/
  @Schema(description = "")
  public Integer getId() {
    return id;
  }

  public BookingtableSchema bookingId(Integer bookingId) {
    this.bookingId = bookingId;
    return this;
  }

   /**
   * Get bookingId
   * @return bookingId
  **/
  @Schema(description = "")
  public Integer getBookingId() {
    return bookingId;
  }

  public void setBookingId(Integer bookingId) {
    this.bookingId = bookingId;
  }

  public BookingtableSchema tableId(String tableId) {
    this.tableId = tableId;
    return this;
  }

   /**
   * Get tableId
   * @return tableId
  **/
  @Schema(description = "")
  public String getTableId() {
    return tableId;
  }

  public void setTableId(String tableId) {
    this.tableId = tableId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BookingtableSchema bookingtableSchema = (BookingtableSchema) o;
    return Objects.equals(this.id, bookingtableSchema.id) &&
        Objects.equals(this.bookingId, bookingtableSchema.bookingId) &&
        Objects.equals(this.tableId, bookingtableSchema.tableId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, bookingId, tableId);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BookingtableSchema {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    bookingId: ").append(toIndentedString(bookingId)).append("\n");
    sb.append("    tableId: ").append(toIndentedString(tableId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
