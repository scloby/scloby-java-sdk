/*
 * Scloby Api
 * **Introduction**  **The authentication token is issued via Oauth2 standard ( https://oauth.net)**    You can apply for a Scloby test account on our developer portal **https://developer.scloby.com**   Here is your **ClientId** and **ClientSecret** for tests:    **client_id**: SclobyApiDocs    **client_secret**: DdyPNPvfPOGa0Izjct0C   **All data is JSON format, and the Content-Type header of POST/PUT request must be set to application/json.**  **Search Parameters**  You can perform a search in a collection using all first level properties in combination with some parameter, or specifying a value:    property = VALUE if you are searching for all items with a specific property value property_since = MIN_VALUE   if you are searching for all items with property value greater (or equal) than MIN_VALUE property_max   = MAX_VALUE   if you are searching for all items with property value smaller (or equal)than MAX_VALUE property_like  = LIKE_VALUE   if you are searching for a substring content You can use property_since and property_max in order to simulate **between** operator   If you are using *equal* operator, all other operators for that parameter *will be ingnored*   If you are using _max or _since operator in strings, the alphabetical ordering will be used
 *
 * OpenAPI spec version: 2.0.0
 * Contact: dev@scloby.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.client.model.OrderitemingredientSchema;
import io.swagger.client.model.OrderitemvariationSchema;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.threeten.bp.OffsetDateTime;
/**
 * OrderitemSchema
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2020-11-03T17:06:12.537Z[GMT]")
public class OrderitemSchema {
  @SerializedName("id")
  private Integer id = null;

  @SerializedName("uuid")
  private String uuid = null;

  @SerializedName("order_id")
  private Integer orderId = null;

  @SerializedName("item_id")
  private Integer itemId = null;

  @SerializedName("name")
  private String name = null;

  @SerializedName("order_name")
  private String orderName = null;

  @SerializedName("category_id")
  private Integer categoryId = null;

  @SerializedName("category_name")
  private String categoryName = null;

  @SerializedName("notes")
  private String notes = null;

  @SerializedName("half_portion")
  private Boolean halfPortion = null;

  @SerializedName("price")
  private BigDecimal price = null;

  @SerializedName("cost")
  private BigDecimal cost = null;

  @SerializedName("net_price")
  private BigDecimal netPrice = null;

  @SerializedName("vat_perc")
  private BigDecimal vatPerc = null;

  @SerializedName("final_price")
  private BigDecimal finalPrice = null;

  @SerializedName("final_net_price")
  private BigDecimal finalNetPrice = null;

  @SerializedName("quantity")
  private Integer quantity = null;

  @SerializedName("unit")
  private String unit = null;

  @SerializedName("exit")
  private Integer exit = null;

  @SerializedName("lastupdate_at")
  private OffsetDateTime lastupdateAt = null;

  @SerializedName("lastupdate_by")
  private Integer lastupdateBy = null;

  @SerializedName("added_at")
  private OffsetDateTime addedAt = null;

  @SerializedName("operator_id")
  private Integer operatorId = null;

  @SerializedName("operator_name")
  private String operatorName = null;

  @SerializedName("department_id")
  private Integer departmentId = null;

  @SerializedName("department_name")
  private String departmentName = null;

  @SerializedName("created_at")
  private OffsetDateTime createdAt = null;

  @SerializedName("updated_at")
  private OffsetDateTime updatedAt = null;

  @SerializedName("deleted_at")
  private OffsetDateTime deletedAt = null;

  @SerializedName("variations")
  private List<OrderitemvariationSchema> variations = null;

  @SerializedName("ingredients")
  private List<OrderitemingredientSchema> ingredients = null;

   /**
   * Get id
   * @return id
  **/
  @Schema(description = "")
  public Integer getId() {
    return id;
  }

  public OrderitemSchema uuid(String uuid) {
    this.uuid = uuid;
    return this;
  }

   /**
   * Get uuid
   * @return uuid
  **/
  @Schema(required = true, description = "")
  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public OrderitemSchema orderId(Integer orderId) {
    this.orderId = orderId;
    return this;
  }

   /**
   * Get orderId
   * @return orderId
  **/
  @Schema(description = "")
  public Integer getOrderId() {
    return orderId;
  }

  public void setOrderId(Integer orderId) {
    this.orderId = orderId;
  }

  public OrderitemSchema itemId(Integer itemId) {
    this.itemId = itemId;
    return this;
  }

   /**
   * Get itemId
   * @return itemId
  **/
  @Schema(description = "")
  public Integer getItemId() {
    return itemId;
  }

  public void setItemId(Integer itemId) {
    this.itemId = itemId;
  }

  public OrderitemSchema name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Original Name of item sold
   * @return name
  **/
  @Schema(description = "Original Name of item sold")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public OrderitemSchema orderName(String orderName) {
    this.orderName = orderName;
    return this;
  }

   /**
   * Name you want to print into kitchen tickets
   * @return orderName
  **/
  @Schema(description = "Name you want to print into kitchen tickets")
  public String getOrderName() {
    return orderName;
  }

  public void setOrderName(String orderName) {
    this.orderName = orderName;
  }

  public OrderitemSchema categoryId(Integer categoryId) {
    this.categoryId = categoryId;
    return this;
  }

   /**
   * Get categoryId
   * @return categoryId
  **/
  @Schema(description = "")
  public Integer getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Integer categoryId) {
    this.categoryId = categoryId;
  }

  public OrderitemSchema categoryName(String categoryName) {
    this.categoryName = categoryName;
    return this;
  }

   /**
   * Get categoryName
   * @return categoryName
  **/
  @Schema(description = "")
  public String getCategoryName() {
    return categoryName;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }

  public OrderitemSchema notes(String notes) {
    this.notes = notes;
    return this;
  }

   /**
   * Get notes
   * @return notes
  **/
  @Schema(description = "")
  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public OrderitemSchema halfPortion(Boolean halfPortion) {
    this.halfPortion = halfPortion;
    return this;
  }

   /**
   * Get halfPortion
   * @return halfPortion
  **/
  @Schema(description = "")
  public Boolean isHalfPortion() {
    return halfPortion;
  }

  public void setHalfPortion(Boolean halfPortion) {
    this.halfPortion = halfPortion;
  }

  public OrderitemSchema price(BigDecimal price) {
    this.price = price;
    return this;
  }

   /**
   * Get price
   * @return price
  **/
  @Schema(required = true, description = "")
  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public OrderitemSchema cost(BigDecimal cost) {
    this.cost = cost;
    return this;
  }

   /**
   * Get cost
   * @return cost
  **/
  @Schema(description = "")
  public BigDecimal getCost() {
    return cost;
  }

  public void setCost(BigDecimal cost) {
    this.cost = cost;
  }

  public OrderitemSchema netPrice(BigDecimal netPrice) {
    this.netPrice = netPrice;
    return this;
  }

   /**
   * Get netPrice
   * @return netPrice
  **/
  @Schema(required = true, description = "")
  public BigDecimal getNetPrice() {
    return netPrice;
  }

  public void setNetPrice(BigDecimal netPrice) {
    this.netPrice = netPrice;
  }

  public OrderitemSchema vatPerc(BigDecimal vatPerc) {
    this.vatPerc = vatPerc;
    return this;
  }

   /**
   * Get vatPerc
   * minimum: 0
   * maximum: 30
   * @return vatPerc
  **/
  @Schema(required = true, description = "")
  public BigDecimal getVatPerc() {
    return vatPerc;
  }

  public void setVatPerc(BigDecimal vatPerc) {
    this.vatPerc = vatPerc;
  }

  public OrderitemSchema finalPrice(BigDecimal finalPrice) {
    this.finalPrice = finalPrice;
    return this;
  }

   /**
   * Price with VAT (ingredients or variations price differences are included)
   * @return finalPrice
  **/
  @Schema(required = true, description = "Price with VAT (ingredients or variations price differences are included)")
  public BigDecimal getFinalPrice() {
    return finalPrice;
  }

  public void setFinalPrice(BigDecimal finalPrice) {
    this.finalPrice = finalPrice;
  }

  public OrderitemSchema finalNetPrice(BigDecimal finalNetPrice) {
    this.finalNetPrice = finalNetPrice;
    return this;
  }

   /**
   * Price without VAT (ingredients or variations price differences are included)
   * @return finalNetPrice
  **/
  @Schema(required = true, description = "Price without VAT (ingredients or variations price differences are included)")
  public BigDecimal getFinalNetPrice() {
    return finalNetPrice;
  }

  public void setFinalNetPrice(BigDecimal finalNetPrice) {
    this.finalNetPrice = finalNetPrice;
  }

  public OrderitemSchema quantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }

   /**
   * Get quantity
   * @return quantity
  **/
  @Schema(required = true, description = "")
  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public OrderitemSchema unit(String unit) {
    this.unit = unit;
    return this;
  }

   /**
   * Get unit
   * @return unit
  **/
  @Schema(description = "")
  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public OrderitemSchema exit(Integer exit) {
    this.exit = exit;
    return this;
  }

   /**
   * Item exit, can be null or from 1 to 10
   * minimum: 1
   * maximum: 10
   * @return exit
  **/
  @Schema(description = "Item exit, can be null or from 1 to 10")
  public Integer getExit() {
    return exit;
  }

  public void setExit(Integer exit) {
    this.exit = exit;
  }

  public OrderitemSchema lastupdateAt(OffsetDateTime lastupdateAt) {
    this.lastupdateAt = lastupdateAt;
    return this;
  }

   /**
   * Get lastupdateAt
   * @return lastupdateAt
  **/
  @Schema(description = "")
  public OffsetDateTime getLastupdateAt() {
    return lastupdateAt;
  }

  public void setLastupdateAt(OffsetDateTime lastupdateAt) {
    this.lastupdateAt = lastupdateAt;
  }

  public OrderitemSchema lastupdateBy(Integer lastupdateBy) {
    this.lastupdateBy = lastupdateBy;
    return this;
  }

   /**
   * Get lastupdateBy
   * @return lastupdateBy
  **/
  @Schema(description = "")
  public Integer getLastupdateBy() {
    return lastupdateBy;
  }

  public void setLastupdateBy(Integer lastupdateBy) {
    this.lastupdateBy = lastupdateBy;
  }

  public OrderitemSchema addedAt(OffsetDateTime addedAt) {
    this.addedAt = addedAt;
    return this;
  }

   /**
   * Get addedAt
   * @return addedAt
  **/
  @Schema(description = "")
  public OffsetDateTime getAddedAt() {
    return addedAt;
  }

  public void setAddedAt(OffsetDateTime addedAt) {
    this.addedAt = addedAt;
  }

  public OrderitemSchema operatorId(Integer operatorId) {
    this.operatorId = operatorId;
    return this;
  }

   /**
   * Get operatorId
   * @return operatorId
  **/
  @Schema(required = true, description = "")
  public Integer getOperatorId() {
    return operatorId;
  }

  public void setOperatorId(Integer operatorId) {
    this.operatorId = operatorId;
  }

  public OrderitemSchema operatorName(String operatorName) {
    this.operatorName = operatorName;
    return this;
  }

   /**
   * Get operatorName
   * @return operatorName
  **/
  @Schema(required = true, description = "")
  public String getOperatorName() {
    return operatorName;
  }

  public void setOperatorName(String operatorName) {
    this.operatorName = operatorName;
  }

  public OrderitemSchema departmentId(Integer departmentId) {
    this.departmentId = departmentId;
    return this;
  }

   /**
   * Get departmentId
   * @return departmentId
  **/
  @Schema(description = "")
  public Integer getDepartmentId() {
    return departmentId;
  }

  public void setDepartmentId(Integer departmentId) {
    this.departmentId = departmentId;
  }

  public OrderitemSchema departmentName(String departmentName) {
    this.departmentName = departmentName;
    return this;
  }

   /**
   * Get departmentName
   * @return departmentName
  **/
  @Schema(description = "")
  public String getDepartmentName() {
    return departmentName;
  }

  public void setDepartmentName(String departmentName) {
    this.departmentName = departmentName;
  }

   /**
   * Get createdAt
   * @return createdAt
  **/
  @Schema(description = "")
  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

   /**
   * Get updatedAt
   * @return updatedAt
  **/
  @Schema(description = "")
  public OffsetDateTime getUpdatedAt() {
    return updatedAt;
  }

   /**
   * Get deletedAt
   * @return deletedAt
  **/
  @Schema(description = "")
  public OffsetDateTime getDeletedAt() {
    return deletedAt;
  }

  public OrderitemSchema variations(List<OrderitemvariationSchema> variations) {
    this.variations = variations;
    return this;
  }

  public OrderitemSchema addVariationsItem(OrderitemvariationSchema variationsItem) {
    if (this.variations == null) {
      this.variations = new ArrayList<OrderitemvariationSchema>();
    }
    this.variations.add(variationsItem);
    return this;
  }

   /**
   * Get variations
   * @return variations
  **/
  @Schema(description = "")
  public List<OrderitemvariationSchema> getVariations() {
    return variations;
  }

  public void setVariations(List<OrderitemvariationSchema> variations) {
    this.variations = variations;
  }

  public OrderitemSchema ingredients(List<OrderitemingredientSchema> ingredients) {
    this.ingredients = ingredients;
    return this;
  }

  public OrderitemSchema addIngredientsItem(OrderitemingredientSchema ingredientsItem) {
    if (this.ingredients == null) {
      this.ingredients = new ArrayList<OrderitemingredientSchema>();
    }
    this.ingredients.add(ingredientsItem);
    return this;
  }

   /**
   * Get ingredients
   * @return ingredients
  **/
  @Schema(description = "")
  public List<OrderitemingredientSchema> getIngredients() {
    return ingredients;
  }

  public void setIngredients(List<OrderitemingredientSchema> ingredients) {
    this.ingredients = ingredients;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OrderitemSchema orderitemSchema = (OrderitemSchema) o;
    return Objects.equals(this.id, orderitemSchema.id) &&
        Objects.equals(this.uuid, orderitemSchema.uuid) &&
        Objects.equals(this.orderId, orderitemSchema.orderId) &&
        Objects.equals(this.itemId, orderitemSchema.itemId) &&
        Objects.equals(this.name, orderitemSchema.name) &&
        Objects.equals(this.orderName, orderitemSchema.orderName) &&
        Objects.equals(this.categoryId, orderitemSchema.categoryId) &&
        Objects.equals(this.categoryName, orderitemSchema.categoryName) &&
        Objects.equals(this.notes, orderitemSchema.notes) &&
        Objects.equals(this.halfPortion, orderitemSchema.halfPortion) &&
        Objects.equals(this.price, orderitemSchema.price) &&
        Objects.equals(this.cost, orderitemSchema.cost) &&
        Objects.equals(this.netPrice, orderitemSchema.netPrice) &&
        Objects.equals(this.vatPerc, orderitemSchema.vatPerc) &&
        Objects.equals(this.finalPrice, orderitemSchema.finalPrice) &&
        Objects.equals(this.finalNetPrice, orderitemSchema.finalNetPrice) &&
        Objects.equals(this.quantity, orderitemSchema.quantity) &&
        Objects.equals(this.unit, orderitemSchema.unit) &&
        Objects.equals(this.exit, orderitemSchema.exit) &&
        Objects.equals(this.lastupdateAt, orderitemSchema.lastupdateAt) &&
        Objects.equals(this.lastupdateBy, orderitemSchema.lastupdateBy) &&
        Objects.equals(this.addedAt, orderitemSchema.addedAt) &&
        Objects.equals(this.operatorId, orderitemSchema.operatorId) &&
        Objects.equals(this.operatorName, orderitemSchema.operatorName) &&
        Objects.equals(this.departmentId, orderitemSchema.departmentId) &&
        Objects.equals(this.departmentName, orderitemSchema.departmentName) &&
        Objects.equals(this.createdAt, orderitemSchema.createdAt) &&
        Objects.equals(this.updatedAt, orderitemSchema.updatedAt) &&
        Objects.equals(this.deletedAt, orderitemSchema.deletedAt) &&
        Objects.equals(this.variations, orderitemSchema.variations) &&
        Objects.equals(this.ingredients, orderitemSchema.ingredients);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, uuid, orderId, itemId, name, orderName, categoryId, categoryName, notes, halfPortion, price, cost, netPrice, vatPerc, finalPrice, finalNetPrice, quantity, unit, exit, lastupdateAt, lastupdateBy, addedAt, operatorId, operatorName, departmentId, departmentName, createdAt, updatedAt, deletedAt, variations, ingredients);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OrderitemSchema {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    uuid: ").append(toIndentedString(uuid)).append("\n");
    sb.append("    orderId: ").append(toIndentedString(orderId)).append("\n");
    sb.append("    itemId: ").append(toIndentedString(itemId)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    orderName: ").append(toIndentedString(orderName)).append("\n");
    sb.append("    categoryId: ").append(toIndentedString(categoryId)).append("\n");
    sb.append("    categoryName: ").append(toIndentedString(categoryName)).append("\n");
    sb.append("    notes: ").append(toIndentedString(notes)).append("\n");
    sb.append("    halfPortion: ").append(toIndentedString(halfPortion)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("    cost: ").append(toIndentedString(cost)).append("\n");
    sb.append("    netPrice: ").append(toIndentedString(netPrice)).append("\n");
    sb.append("    vatPerc: ").append(toIndentedString(vatPerc)).append("\n");
    sb.append("    finalPrice: ").append(toIndentedString(finalPrice)).append("\n");
    sb.append("    finalNetPrice: ").append(toIndentedString(finalNetPrice)).append("\n");
    sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
    sb.append("    unit: ").append(toIndentedString(unit)).append("\n");
    sb.append("    exit: ").append(toIndentedString(exit)).append("\n");
    sb.append("    lastupdateAt: ").append(toIndentedString(lastupdateAt)).append("\n");
    sb.append("    lastupdateBy: ").append(toIndentedString(lastupdateBy)).append("\n");
    sb.append("    addedAt: ").append(toIndentedString(addedAt)).append("\n");
    sb.append("    operatorId: ").append(toIndentedString(operatorId)).append("\n");
    sb.append("    operatorName: ").append(toIndentedString(operatorName)).append("\n");
    sb.append("    departmentId: ").append(toIndentedString(departmentId)).append("\n");
    sb.append("    departmentName: ").append(toIndentedString(departmentName)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("    deletedAt: ").append(toIndentedString(deletedAt)).append("\n");
    sb.append("    variations: ").append(toIndentedString(variations)).append("\n");
    sb.append("    ingredients: ").append(toIndentedString(ingredients)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
