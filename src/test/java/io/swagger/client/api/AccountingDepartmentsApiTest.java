/*
 * Scloby Api
 * **Introduction**  **The authentication token is issued via Oauth2 standard ( https://oauth.net)**    You can apply for a Scloby test account on our developer portal **https://developer.scloby.com**   Here is your **ClientId** and **ClientSecret** for tests:    **client_id**: SclobyApiDocs    **client_secret**: DdyPNPvfPOGa0Izjct0C   **All data is JSON format, and the Content-Type header of POST/PUT request must be set to application/json.**  **Search Parameters**  You can perform a search in a collection using all first level properties in combination with some parameter, or specifying a value:    property = VALUE if you are searching for all items with a specific property value property_since = MIN_VALUE   if you are searching for all items with property value greater (or equal) than MIN_VALUE property_max   = MAX_VALUE   if you are searching for all items with property value smaller (or equal)than MAX_VALUE property_like  = LIKE_VALUE   if you are searching for a substring content You can use property_since and property_max in order to simulate **between** operator   If you are using *equal* operator, all other operators for that parameter *will be ingnored*   If you are using _max or _since operator in strings, the alphabetical ordering will be used
 *
 * OpenAPI spec version: 2.0.0
 * Contact: dev@scloby.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.Departments;
import io.swagger.client.model.StockMovements;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for AccountingDepartmentsApi
 */
@Ignore
public class AccountingDepartmentsApiTest {

    private final AccountingDepartmentsApi api = new AccountingDepartmentsApi();

    /**
     * Get All Departments
     *
     * Returns a Json with data about all Departments (&#x27;Reparti&#x27; in Italy) of selected shop.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void departmentsGetTest() throws ApiException {
        Boolean pagination = null;
        Integer perPage = null;
        Integer page = null;
        Departments response = api.departmentsGet(pagination, perPage, page);

        // TODO: test validations
    }
    /**
     * Delete existing Department
     *
     * In this case you must specify the id in the URL, but it is no necessary in the request body
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void departmentsIdDeleteTest() throws ApiException {
        String id = null;
        api.departmentsIdDelete(id);

        // TODO: test validations
    }
    /**
     * Get existing department
     *
     * In this case you must specify the id in the URL
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void departmentsIdGetTest() throws ApiException {
        String id = null;
        Departments response = api.departmentsIdGet(id);

        // TODO: test validations
    }
    /**
     * Edit existing Department
     *
     * In this case you must specify the id in the URL and change the data you wanna update
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void departmentsIdPutTest() throws ApiException {
        StockMovements body = null;
        String id = null;
        Departments response = api.departmentsIdPut(body, id);

        // TODO: test validations
    }
    /**
     * Add new Department
     *
     * Each Department **require an associated VAT Rate**. First of add or edit a department, you must know the associated VAT Rate ID.  NB: You have to specify the **id** you want to assign, you can have at maximum 10 departments (id max 10).
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void departmentsPostTest() throws ApiException {
        Departments body = null;
        Departments response = api.departmentsPost(body);

        // TODO: test validations
    }
}
