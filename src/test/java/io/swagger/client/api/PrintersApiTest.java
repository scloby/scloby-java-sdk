/*
 * Scloby Api
 * **Introduction**  **The authentication token is issued via Oauth2 standard ( https://oauth.net)**    You can apply for a Scloby test account on our developer portal **https://developer.scloby.com**   Here is your **ClientId** and **ClientSecret** for tests:    **client_id**: SclobyApiDocs    **client_secret**: DdyPNPvfPOGa0Izjct0C   **All data is JSON format, and the Content-Type header of POST/PUT request must be set to application/json.**  **Search Parameters**  You can perform a search in a collection using all first level properties in combination with some parameter, or specifying a value:    property = VALUE if you are searching for all items with a specific property value property_since = MIN_VALUE   if you are searching for all items with property value greater (or equal) than MIN_VALUE property_max   = MAX_VALUE   if you are searching for all items with property value smaller (or equal)than MAX_VALUE property_like  = LIKE_VALUE   if you are searching for a substring content You can use property_since and property_max in order to simulate **between** operator   If you are using *equal* operator, all other operators for that parameter *will be ingnored*   If you are using _max or _since operator in strings, the alphabetical ordering will be used
 *
 * OpenAPI spec version: 2.0.0
 * Contact: dev@scloby.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.Printers;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for PrintersApi
 */
@Ignore
public class PrintersApiTest {

    private final PrintersApi api = new PrintersApi();

    /**
     * Get All Printers
     *
     * Returns a Json with data about all Printers of selected shop.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void printersGetTest() throws ApiException {
        Boolean pagination = null;
        Integer perPage = null;
        Integer page = null;
        Printers response = api.printersGet(pagination, perPage, page);

        // TODO: test validations
    }
    /**
     * Delete existing Printer
     *
     * In this case you must specify the id in the URL, but it is no necessary in the request body
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void printersIdDeleteTest() throws ApiException {
        String id = null;
        api.printersIdDelete(id);

        // TODO: test validations
    }
    /**
     * Get existing Printer
     *
     * In this case you must specify the id in the URL
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void printersIdGetTest() throws ApiException {
        String id = null;
        Printers response = api.printersIdGet(id);

        // TODO: test validations
    }
    /**
     * Edit existing Printer
     *
     * In this case you must specify the id in the URL and change the data you wanna update
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void printersIdPutTest() throws ApiException {
        Printers body = null;
        String id = null;
        Printers response = api.printersIdPut(body, id);

        // TODO: test validations
    }
    /**
     * Add new Printer
     *
     * Returns a Json with the data of the new Printer
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void printersPostTest() throws ApiException {
        Printers body = null;
        Printers response = api.printersPost(body);

        // TODO: test validations
    }
}
