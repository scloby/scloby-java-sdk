# BookingShifts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**shiftId** | **Integer** |  |  [optional]
**customerId** | **Integer** |  | 
**source** | **String** |  |  [optional]
**bookedFor** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**duration** | **Integer** |  | 
**status** | **String** |  | 
**people** | **Integer** |  |  [optional]
**notes** | **String** |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**createdbyId** | **Integer** |  |  [optional]
**updatedbyId** | **Integer** |  |  [optional]
**deletedbyId** | **Integer** |  |  [optional]
**tags** | [**List&lt;BookingtagSchema&gt;**](BookingtagSchema.md) |  |  [optional]
**tables** | [**List&lt;BookingtableSchema&gt;**](BookingtableSchema.md) |  |  [optional]
**customer** | [**CustomerSchema1**](CustomerSchema1.md) |  |  [optional]
