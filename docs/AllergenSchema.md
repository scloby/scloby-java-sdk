# AllergenSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**name** | **String** | Allergen name | 
**description** | **String** |  |  [optional]
