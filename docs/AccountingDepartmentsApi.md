# AccountingDepartmentsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**departmentsGet**](AccountingDepartmentsApi.md#departmentsGet) | **GET** /departments | Get All Departments
[**departmentsIdDelete**](AccountingDepartmentsApi.md#departmentsIdDelete) | **DELETE** /departments/{id} | Delete existing Department
[**departmentsIdGet**](AccountingDepartmentsApi.md#departmentsIdGet) | **GET** /departments/{id} | Get existing department
[**departmentsIdPut**](AccountingDepartmentsApi.md#departmentsIdPut) | **PUT** /departments/{id} | Edit existing Department
[**departmentsPost**](AccountingDepartmentsApi.md#departmentsPost) | **POST** /departments | Add new Department

<a name="departmentsGet"></a>
# **departmentsGet**
> Departments departmentsGet(pagination, perPage, page)

Get All Departments

Returns a Json with data about all Departments (&#x27;Reparti&#x27; in Italy) of selected shop.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AccountingDepartmentsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

AccountingDepartmentsApi apiInstance = new AccountingDepartmentsApi();
Boolean pagination = true; // Boolean | Pagination parameter
Integer perPage = 56; // Integer | Results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    Departments result = apiInstance.departmentsGet(pagination, perPage, page);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountingDepartmentsApi#departmentsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional]
 **perPage** | **Integer**| Results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

[**Departments**](Departments.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="departmentsIdDelete"></a>
# **departmentsIdDelete**
> departmentsIdDelete(id)

Delete existing Department

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AccountingDepartmentsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

AccountingDepartmentsApi apiInstance = new AccountingDepartmentsApi();
String id = "id_example"; // String | id of the Department that need to be deleted
try {
    apiInstance.departmentsIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountingDepartmentsApi#departmentsIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Department that need to be deleted |

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="departmentsIdGet"></a>
# **departmentsIdGet**
> Departments departmentsIdGet(id)

Get existing department

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AccountingDepartmentsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

AccountingDepartmentsApi apiInstance = new AccountingDepartmentsApi();
String id = "id_example"; // String | id of the department
try {
    Departments result = apiInstance.departmentsIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountingDepartmentsApi#departmentsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the department |

### Return type

[**Departments**](Departments.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="departmentsIdPut"></a>
# **departmentsIdPut**
> Departments departmentsIdPut(body, id)

Edit existing Department

In this case you must specify the id in the URL and change the data you wanna update

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AccountingDepartmentsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

AccountingDepartmentsApi apiInstance = new AccountingDepartmentsApi();
StockMovements body = new StockMovements(); // StockMovements | Object data that need to be updated
String id = "id_example"; // String | id of the department that need to be updated
try {
    Departments result = apiInstance.departmentsIdPut(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountingDepartmentsApi#departmentsIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**StockMovements**](StockMovements.md)| Object data that need to be updated |
 **id** | **String**| id of the department that need to be updated |

### Return type

[**Departments**](Departments.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="departmentsPost"></a>
# **departmentsPost**
> Departments departmentsPost(body)

Add new Department

Each Department **require an associated VAT Rate**. First of add or edit a department, you must know the associated VAT Rate ID.  NB: You have to specify the **id** you want to assign, you can have at maximum 10 departments (id max 10).

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AccountingDepartmentsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

AccountingDepartmentsApi apiInstance = new AccountingDepartmentsApi();
Departments body = new Departments(); // Departments | Department object that needs to be added.
try {
    Departments result = apiInstance.departmentsPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountingDepartmentsApi#departmentsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Departments**](Departments.md)| Department object that needs to be added. |

### Return type

[**Departments**](Departments.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

