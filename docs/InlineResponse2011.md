# InlineResponse2011

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gender** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**id** | **Integer** |  |  [optional]
**companyName** | **String** |  |  [optional]
**firstName** | **String** |  |  [optional]
**lastName** | **String** |  |  [optional]
**uuid** | **String** |  |  [optional]
**birthdate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**billingStreet** | **String** |  |  [optional]
**billingNumber** | **String** |  |  [optional]
**billingCity** | **String** |  |  [optional]
**billingZip** | **String** |  |  [optional]
**billingProv** | **String** |  |  [optional]
**shippingStreet** | **String** |  |  [optional]
**shippingNumber** | **String** |  |  [optional]
**shippingCity** | **String** |  |  [optional]
**shippingProv** | **String** |  |  [optional]
**phone** | **String** |  |  [optional]
**mobile** | **String** |  |  [optional]
**email** | **String** |  |  [optional]
**emailPec** | **String** |  |  [optional]
**taxCode** | **String** |  |  [optional]
