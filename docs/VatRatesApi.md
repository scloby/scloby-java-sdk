# VatRatesApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**vatGet**](VatRatesApi.md#vatGet) | **GET** /vat | Get All VAT Rates
[**vatIdDelete**](VatRatesApi.md#vatIdDelete) | **DELETE** /vat/{id} | Delete existing Vat rate
[**vatIdGet**](VatRatesApi.md#vatIdGet) | **GET** /vat/{id} | Get existing Vat
[**vatIdPut**](VatRatesApi.md#vatIdPut) | **PUT** /vat/{id} | Edit existing Vat
[**vatPost**](VatRatesApi.md#vatPost) | **POST** /vat | Add new Vat rate

<a name="vatGet"></a>
# **vatGet**
> Vat vatGet()

Get All VAT Rates

Returns a Json with data about all vat rates (&#x27;Aliquote IVA&#x27; in Italy) of selected shop.  VAT Rates can be 4 at maximum.  Not Paginated by default

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VatRatesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

VatRatesApi apiInstance = new VatRatesApi();
try {
    Vat result = apiInstance.vatGet();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VatRatesApi#vatGet");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Vat**](Vat.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="vatIdDelete"></a>
# **vatIdDelete**
> vatIdDelete(id)

Delete existing Vat rate

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VatRatesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

VatRatesApi apiInstance = new VatRatesApi();
String id = "id_example"; // String | id of the Vat that need to be deleted
try {
    apiInstance.vatIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling VatRatesApi#vatIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Vat that need to be deleted |

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="vatIdGet"></a>
# **vatIdGet**
> Vat vatIdGet(id)

Get existing Vat

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VatRatesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

VatRatesApi apiInstance = new VatRatesApi();
String id = "id_example"; // String | id of the vat
try {
    Vat result = apiInstance.vatIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VatRatesApi#vatIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the vat |

### Return type

[**Vat**](Vat.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="vatIdPut"></a>
# **vatIdPut**
> Vat vatIdPut(body, id)

Edit existing Vat

In this case you must specify the id in the URL and change the data you wanna update

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VatRatesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

VatRatesApi apiInstance = new VatRatesApi();
Vat body = new Vat(); // Vat | Object data that need to be updated
String id = "id_example"; // String | id of the Vat that need to be updated
try {
    Vat result = apiInstance.vatIdPut(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VatRatesApi#vatIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Vat**](Vat.md)| Object data that need to be updated |
 **id** | **String**| id of the Vat that need to be updated |

### Return type

[**Vat**](Vat.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="vatPost"></a>
# **vatPost**
> Vat vatPost(body)

Add new Vat rate

NB: You have to specify the **id** you want to assign.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VatRatesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

VatRatesApi apiInstance = new VatRatesApi();
Vat body = new Vat(); // Vat | Vat object that needs to be added.
try {
    Vat result = apiInstance.vatPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VatRatesApi#vatPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Vat**](Vat.md)| Vat object that needs to be added. |

### Return type

[**Vat**](Vat.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

