# InlineResponse400Error

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  |  [optional]
**code** | **Integer** |  |  [optional]
