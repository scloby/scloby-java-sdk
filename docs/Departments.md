# Departments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | 
**name** | **String** |  | 
**lawRef** | **String** |  |  [optional]
**vatId** | **Integer** |  | 
**notDiscountable** | **Boolean** |  |  [optional]
**vat** | [**VatSchema**](VatSchema.md) |  |  [optional]
