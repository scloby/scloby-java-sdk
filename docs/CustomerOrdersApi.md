# CustomerOrdersApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ordersGet**](CustomerOrdersApi.md#ordersGet) | **GET** /orders | Get All Orders
[**ordersIdDelete**](CustomerOrdersApi.md#ordersIdDelete) | **DELETE** /orders/{id} | Delete existing Order
[**ordersIdGet**](CustomerOrdersApi.md#ordersIdGet) | **GET** /orders/{id} | Get existing Order
[**ordersIdPut**](CustomerOrdersApi.md#ordersIdPut) | **PUT** /orders/{id} | Edit existing Order
[**ordersPost**](CustomerOrdersApi.md#ordersPost) | **POST** /orders | Add new Order

<a name="ordersGet"></a>
# **ordersGet**
> Orders ordersGet(pagination, perPage, page)

Get All Orders

Returns a Json with all orders of selected shop.  Paginated by default (per_page&#x3D;1000).    **Orders status**  An order can be in open or closed status. If is open it can be edited. Each order can contain a &#x27;previus_order&#x27; that is the previous version of the order, before last save (parcheggia) or print.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomerOrdersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

CustomerOrdersApi apiInstance = new CustomerOrdersApi();
Boolean pagination = true; // Boolean | Pagination parameter
Integer perPage = 56; // Integer | Results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    Orders result = apiInstance.ordersGet(pagination, perPage, page);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerOrdersApi#ordersGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional]
 **perPage** | **Integer**| Results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

[**Orders**](Orders.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="ordersIdDelete"></a>
# **ordersIdDelete**
> ordersIdDelete(id)

Delete existing Order

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomerOrdersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

CustomerOrdersApi apiInstance = new CustomerOrdersApi();
String id = "id_example"; // String | id of the Order that need to be deleted
try {
    apiInstance.ordersIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerOrdersApi#ordersIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Order that need to be deleted |

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="ordersIdGet"></a>
# **ordersIdGet**
> Orders ordersIdGet(id)

Get existing Order

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomerOrdersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

CustomerOrdersApi apiInstance = new CustomerOrdersApi();
String id = "id_example"; // String | id of the order
try {
    Orders result = apiInstance.ordersIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerOrdersApi#ordersIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the order |

### Return type

[**Orders**](Orders.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="ordersIdPut"></a>
# **ordersIdPut**
> Orders ordersIdPut(body, id)

Edit existing Order

In this case you must specify the id in the URL and change the data you wanna update

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomerOrdersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

CustomerOrdersApi apiInstance = new CustomerOrdersApi();
Orders body = new Orders(); // Orders | Object data that need to be updated
String id = "id_example"; // String | id of the Order that need to be updated
try {
    Orders result = apiInstance.ordersIdPut(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerOrdersApi#ordersIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Orders**](Orders.md)| Object data that need to be updated |
 **id** | **String**| id of the Order that need to be updated |

### Return type

[**Orders**](Orders.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="ordersPost"></a>
# **ordersPost**
> Orders ordersPost(body)

Add new Order

Returns a Json with the data of the new Order

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomerOrdersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

CustomerOrdersApi apiInstance = new CustomerOrdersApi();
Orders body = new Orders(); // Orders | Order object that needs to be added.
try {
    Orders result = apiInstance.ordersPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerOrdersApi#ordersPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Orders**](Orders.md)| Order object that needs to be added. |

### Return type

[**Orders**](Orders.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

