# ChainsRules

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**name** | **String** |  | 
**type** | **String** |  | 
**minThreshold** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**maxThreshold** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**excludeToMin** | **Boolean** |  |  [optional]
**points** | **Integer** |  | 
**param1** | **String** |  |  [optional]
**param2** | **String** |  |  [optional]
**param3** | **String** |  |  [optional]
