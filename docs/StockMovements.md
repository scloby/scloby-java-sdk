# StockMovements

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**itemId** | **Integer** |  |  [optional]
**combinationId** | **Integer** |  |  [optional]
**rawMaterialId** | **Integer** |  |  [optional]
**entryType** | **String** |  | 
**name** | **String** |  |  [optional]
**barcode** | **String** |  |  [optional]
**code** | **String** |  |  [optional]
**optionsValues** | **String** |  |  [optional]
**combination** | **String** |  |  [optional]
**supplierId** | **Integer** |  |  [optional]
**supplierName** | **String** |  |  [optional]
**supplierOrder** | **String** |  |  [optional]
**saleId** | **Integer** |  |  [optional]
**quantity** | [**BigDecimal**](BigDecimal.md) |  | 
**unit** | **String** |  |  [optional]
**type** | **String** |  | 
**loadCause** | **String** |  |  [optional]
**unloadCause** | **String** |  |  [optional]
**operatorId** | **Integer** |  |  [optional]
**operatorUsername** | **String** |  |  [optional]
**operatorFullname** | **String** |  |  [optional]
**date** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**notes** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**createdbyId** | **Integer** |  |  [optional]
**updatedbyId** | **Integer** |  |  [optional]
**deletedbyId** | **Integer** |  |  [optional]
**operatorName** | **Object** |  |  [optional]
