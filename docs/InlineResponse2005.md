# InlineResponse2005

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**dbName** | **String** |  |  [optional]
**shopName** | **String** |  |  [optional]
**uuid** | **String** |  |  [optional]
