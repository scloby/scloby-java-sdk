# SalesApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**salesGet**](SalesApi.md#salesGet) | **GET** /sales | Get All Sales
[**salesIdDelete**](SalesApi.md#salesIdDelete) | **DELETE** /sales/{id} | Delete existing Sale
[**salesIdGet**](SalesApi.md#salesIdGet) | **GET** /sales/{id} | Get existing Sale
[**salesIdPut**](SalesApi.md#salesIdPut) | **PUT** /sales/{id} | Edit existing Sale
[**salesPost**](SalesApi.md#salesPost) | **POST** /sales | Add new Sale

<a name="salesGet"></a>
# **salesGet**
> Sales salesGet(pagination, perPage, page)

Get All Sales

Returns a Json with data about all sales of selected shop.  Paginated by default (per_page&#x3D;1000)

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SalesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

SalesApi apiInstance = new SalesApi();
Boolean pagination = true; // Boolean | Pagination parameter
Integer perPage = 56; // Integer | Results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    Sales result = apiInstance.salesGet(pagination, perPage, page);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SalesApi#salesGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional]
 **perPage** | **Integer**| Results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

[**Sales**](Sales.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="salesIdDelete"></a>
# **salesIdDelete**
> salesIdDelete(id)

Delete existing Sale

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SalesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

SalesApi apiInstance = new SalesApi();
String id = "id_example"; // String | id of the Sale that need to be deleted
try {
    apiInstance.salesIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling SalesApi#salesIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Sale that need to be deleted |

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="salesIdGet"></a>
# **salesIdGet**
> Sales salesIdGet(id)

Get existing Sale

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SalesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

SalesApi apiInstance = new SalesApi();
String id = "id_example"; // String | id of the sale
try {
    Sales result = apiInstance.salesIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SalesApi#salesIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the sale |

### Return type

[**Sales**](Sales.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="salesIdPut"></a>
# **salesIdPut**
> Sales salesIdPut(body, id)

Edit existing Sale

In this case you must specify the id in the URL and change the data you wanna update

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SalesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

SalesApi apiInstance = new SalesApi();
Sales body = new Sales(); // Sales | Object data that need to be updated
String id = "id_example"; // String | id of the Sale that need to be updated
try {
    Sales result = apiInstance.salesIdPut(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SalesApi#salesIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Sales**](Sales.md)| Object data that need to be updated |
 **id** | **String**| id of the Sale that need to be updated |

### Return type

[**Sales**](Sales.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="salesPost"></a>
# **salesPost**
> Sales salesPost(body)

Add new Sale

Returns a Json with the data of the new Sale

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SalesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

SalesApi apiInstance = new SalesApi();
Sales body = new Sales(); // Sales | **Sale.sale_items attributes explanaion**

A sale contains an array named sale_items, that contains the items (and its discount/surcharges) you want to sell.



Each item has the following important attributes:

- **department_id**: The integer id of department (see /departments endpoint) related to this item, it is very important because the italian fiscal printers want to know exactly which is the department for each item sold, because for each department there is a VAT percentage ( see /vat endpoint ) that the printer know for its internal fiscal aggregates.

- **department_name**: The name of related department.

- **vat_perc**: The VAT percentage for this related department.

- **item_id**: The integer id of item sold (not necessary but useful).

- **name**: The name of the item sold,

- **price**: Unit price of this item (before surcharge and discount).

- **quantity**: Sold quantity of this item, can be decimal and negative in refund case.

- **type**: Can be 'Sale' (quantity > 0), 'refund' (quantity < 0) or 'gift' (price = 0)

- **uuid**: That identifies univocally the sale item (in UUID standard format).

- **price_changes**: Each sale item can have an array of discount/surcharges that is called price_changes, and is applied to price in the order specified by each index.Each price_change is composed by:

 1. **description**: A textual description of this discount/surcharge.

 2. **index**: An integer number that is used for order apply.

 3. **type**: Can be: 

 - **discount_fix**: Is if a fixed value discount, for example a discount of 1.00 €.

- **discount_perc**: If is a percentage discount, for example 10 % discount.

 - **surcharge_fix**: If is a fixed value surcharge, for example a surcharge of 1.00 €.

 - **surcharge_perc**: If is a percentage value surchage, for example a surcharge of 10 %. 

 4. **value**: Value of this discount/surcharge.

 Example:

 - Case of 1.00 € discount/surcharge (discount_fix/surcharge_fix), 'value':1.00

- Case of 10 % discount/surcharge (discount_perc/surcharge_perc), 'value':10.00

 - **final_price**:  Unit price, with applied its surcharge and discounts and a quota of global discount/surcharges (in sale document), VAT included Example. 

  If a sale item has quantity 2, a price and 2 price_changes: 1st element discount_perc (index = 1) and 2nd element surcharge_fix (index = 0), and 1 global discount_fix and 1 global discount_perc



 var partial_final_price = sale_item.price sale_item.quantity

 [ sale_item.price_changes[1].index = 0 ] partial_final_price = partial_final_price + sale_item.price_changes[1].value

  [ sale_item.price_changes[1].index = 1 ] partial_final_price = partial_final_price ( 1 - sale_item.price_changes[0].value/100 )

Partial final price with global discount/surcharge quota:var total_discounted = sale.amount - sale.price_changes[0].value

   total_discounted += sale.amount - (sale.amount * sale.price_changes[1].value/100)

   var total_discount = sale.amount - total_discounted

 partial_final_price = partial_final_price - (total_discount * partial_final_price / sale.amount)sale_item.final_price = partial_final_price / sale_item.quantity


- final_net_price: Unit price, with applied surcharge and discounts, VAT excluded sale_item.final_net_price = sale_item.final_price / ( 1 + sale_item.vat_perc)

 **Sale attributes explanation**

A sale is composed by a group of general attributes, in particular we must put attention on sale amounts, global discounts and surcharges:

 - **price_changes**: Each sale can have an array of discount/surcharges that is called price_changes, and is applied to amount in the order specified by each index.

Each price_change is composed by:

 1. **description**: A textual description of this discount/surcharge.

2. **index**: An integer number that is used for order apply.

 3. **type**: can be: 

 **discount_fix**: Is if a fixed value discount, for example a discount of 1.00 €

**discount_perc**: If is a percentage discount, for example 10 % discount

**surcharge_fix**: If is a fixed value surcharge, for example a surcharge of 1.00 €.

**surcharge_perc**: If is a percentage value surchage, for example a surcharge of 10 %


4. **value**: Value of this discount/surcharge. Example: 


  1. Case of 1.00 € discount/surcharge (discount_fix/surcharge_fix), 'value':1.00

 2.Case of 10 % discount/surcharge (discount_perc/surcharge_perc), 'value':10.00

 **amount**: Sum of price quantity + discount/surcharges of all sale_items, global discount/surcharges (that is applied on amount*) is excluded.

 Example 1: If a sale has 3 items sale_items in sale items array, each with 1 discount_fix:

  sale.amount = sale.sale_items[0].quantity * sale.sale_items[0].price - sale.sale_items[0].price_changes[0].value + sale.sale_items[1].quantity * sale.sale_items[1].price - sale.sale_items[1].price_changes[0].value +sale.sale_items[2].quantity * sale.sale_items[2].price - sale.sale_items[0].price_changes[0].value

 Example 2:If a sale has 2 items sale_items in sale items array, each with 1 discount_perc (index = 0) and 1 surcharge_fix (index = 1):

var partial_item_1 = sale.sale_items[0].quantity * sale.sale_items[0].price partial_item_1 = partial_item_1 - sale.sale_items[0].price_changes[0].value partial_item_1 = partial_item_1 * ( 1 + sale.sale_items[0].price_changes[1].value/10 )

  var partial_item_2 = sale.sale_items[1].quantity * sale.sale_items[1].price partial_item_2 = partial_item_2 - sale.sale_items[1].price_changes[0].value partial_item_2 = partial_item_2 * ( 1 + sale.sale_items[1].price_changes[1].value/100 ) 

 sale.amount = partial_item_1 + partial_item_2


 - **final_amount**: Sum of (price quantity) + discount/surcharges of all sale_items, global discount/surcharges (that is applied on amount*) is included.

 Example 1A) If a sale has 3 items sale_items in sale items array, each with 1 discount_fix (like example 1), and 3 global price_changes (first with type = discount_perc and index = 0, second with type = surcharge_fix and index = 1, third with type = surcharge_perc and index = 2):

 sale.final_amount = sale.amount * ( 1 - sale.price_changes[0].value/100)

sale.final_amount = sale.final_amount + sale.price_change[1].value

sale.final_amount = sale.final_amount * ( 1 + sale.price_change[1].value/100 )


 **final_net_amount**: Is the sum of each sale_item_net_price.


Sale POST Data example
try {
    Sales result = apiInstance.salesPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SalesApi#salesPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Sales**](Sales.md)| **Sale.sale_items attributes explanaion**

A sale contains an array named sale_items, that contains the items (and its discount/surcharges) you want to sell.



Each item has the following important attributes:

- **department_id**: The integer id of department (see /departments endpoint) related to this item, it is very important because the italian fiscal printers want to know exactly which is the department for each item sold, because for each department there is a VAT percentage ( see /vat endpoint ) that the printer know for its internal fiscal aggregates.

- **department_name**: The name of related department.

- **vat_perc**: The VAT percentage for this related department.

- **item_id**: The integer id of item sold (not necessary but useful).

- **name**: The name of the item sold,

- **price**: Unit price of this item (before surcharge and discount).

- **quantity**: Sold quantity of this item, can be decimal and negative in refund case.

- **type**: Can be &#x27;Sale&#x27; (quantity &gt; 0), &#x27;refund&#x27; (quantity &lt; 0) or &#x27;gift&#x27; (price &#x3D; 0)

- **uuid**: That identifies univocally the sale item (in UUID standard format).

- **price_changes**: Each sale item can have an array of discount/surcharges that is called price_changes, and is applied to price in the order specified by each index.Each price_change is composed by:

 1. **description**: A textual description of this discount/surcharge.

 2. **index**: An integer number that is used for order apply.

 3. **type**: Can be: 

 - **discount_fix**: Is if a fixed value discount, for example a discount of 1.00 €.

- **discount_perc**: If is a percentage discount, for example 10 % discount.

 - **surcharge_fix**: If is a fixed value surcharge, for example a surcharge of 1.00 €.

 - **surcharge_perc**: If is a percentage value surchage, for example a surcharge of 10 %. 

 4. **value**: Value of this discount/surcharge.

 Example:

 - Case of 1.00 € discount/surcharge (discount_fix/surcharge_fix), &#x27;value&#x27;:1.00

- Case of 10 % discount/surcharge (discount_perc/surcharge_perc), &#x27;value&#x27;:10.00

 - **final_price**:  Unit price, with applied its surcharge and discounts and a quota of global discount/surcharges (in sale document), VAT included Example. 

  If a sale item has quantity 2, a price and 2 price_changes: 1st element discount_perc (index &#x3D; 1) and 2nd element surcharge_fix (index &#x3D; 0), and 1 global discount_fix and 1 global discount_perc



 var partial_final_price &#x3D; sale_item.price sale_item.quantity

 [ sale_item.price_changes[1].index &#x3D; 0 ] partial_final_price &#x3D; partial_final_price + sale_item.price_changes[1].value

  [ sale_item.price_changes[1].index &#x3D; 1 ] partial_final_price &#x3D; partial_final_price ( 1 - sale_item.price_changes[0].value/100 )

Partial final price with global discount/surcharge quota:var total_discounted &#x3D; sale.amount - sale.price_changes[0].value

   total_discounted +&#x3D; sale.amount - (sale.amount * sale.price_changes[1].value/100)

   var total_discount &#x3D; sale.amount - total_discounted

 partial_final_price &#x3D; partial_final_price - (total_discount * partial_final_price / sale.amount)sale_item.final_price &#x3D; partial_final_price / sale_item.quantity


- final_net_price: Unit price, with applied surcharge and discounts, VAT excluded sale_item.final_net_price &#x3D; sale_item.final_price / ( 1 + sale_item.vat_perc)

 **Sale attributes explanation**

A sale is composed by a group of general attributes, in particular we must put attention on sale amounts, global discounts and surcharges:

 - **price_changes**: Each sale can have an array of discount/surcharges that is called price_changes, and is applied to amount in the order specified by each index.

Each price_change is composed by:

 1. **description**: A textual description of this discount/surcharge.

2. **index**: An integer number that is used for order apply.

 3. **type**: can be: 

 **discount_fix**: Is if a fixed value discount, for example a discount of 1.00 €

**discount_perc**: If is a percentage discount, for example 10 % discount

**surcharge_fix**: If is a fixed value surcharge, for example a surcharge of 1.00 €.

**surcharge_perc**: If is a percentage value surchage, for example a surcharge of 10 %


4. **value**: Value of this discount/surcharge. Example: 


  1. Case of 1.00 € discount/surcharge (discount_fix/surcharge_fix), &#x27;value&#x27;:1.00

 2.Case of 10 % discount/surcharge (discount_perc/surcharge_perc), &#x27;value&#x27;:10.00

 **amount**: Sum of price quantity + discount/surcharges of all sale_items, global discount/surcharges (that is applied on amount*) is excluded.

 Example 1: If a sale has 3 items sale_items in sale items array, each with 1 discount_fix:

  sale.amount &#x3D; sale.sale_items[0].quantity * sale.sale_items[0].price - sale.sale_items[0].price_changes[0].value + sale.sale_items[1].quantity * sale.sale_items[1].price - sale.sale_items[1].price_changes[0].value +sale.sale_items[2].quantity * sale.sale_items[2].price - sale.sale_items[0].price_changes[0].value

 Example 2:If a sale has 2 items sale_items in sale items array, each with 1 discount_perc (index &#x3D; 0) and 1 surcharge_fix (index &#x3D; 1):

var partial_item_1 &#x3D; sale.sale_items[0].quantity * sale.sale_items[0].price partial_item_1 &#x3D; partial_item_1 - sale.sale_items[0].price_changes[0].value partial_item_1 &#x3D; partial_item_1 * ( 1 + sale.sale_items[0].price_changes[1].value/10 )

  var partial_item_2 &#x3D; sale.sale_items[1].quantity * sale.sale_items[1].price partial_item_2 &#x3D; partial_item_2 - sale.sale_items[1].price_changes[0].value partial_item_2 &#x3D; partial_item_2 * ( 1 + sale.sale_items[1].price_changes[1].value/100 ) 

 sale.amount &#x3D; partial_item_1 + partial_item_2


 - **final_amount**: Sum of (price quantity) + discount/surcharges of all sale_items, global discount/surcharges (that is applied on amount*) is included.

 Example 1A) If a sale has 3 items sale_items in sale items array, each with 1 discount_fix (like example 1), and 3 global price_changes (first with type &#x3D; discount_perc and index &#x3D; 0, second with type &#x3D; surcharge_fix and index &#x3D; 1, third with type &#x3D; surcharge_perc and index &#x3D; 2):

 sale.final_amount &#x3D; sale.amount * ( 1 - sale.price_changes[0].value/100)

sale.final_amount &#x3D; sale.final_amount + sale.price_change[1].value

sale.final_amount &#x3D; sale.final_amount * ( 1 + sale.price_change[1].value/100 )


 **final_net_amount**: Is the sum of each sale_item_net_price.


Sale POST Data example |

### Return type

[**Sales**](Sales.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

