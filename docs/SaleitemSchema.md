# SaleitemSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**externalId** | **String** |  |  [optional]
**saleId** | **Integer** |  |  [optional]
**referenceSequentialNumber** | **Integer** |  |  [optional]
**referenceText** | **String** |  |  [optional]
**referenceDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**uuid** | **String** |  | 
**itemId** | **Integer** |  |  [optional]
**combinationId** | **Integer** |  |  [optional]
**prizeId** | **Integer** |  |  [optional]
**type** | **String** |  | 
**refundCauseId** | **Integer** |  |  [optional]
**refundCauseDescription** | **String** |  |  [optional]
**name** | **String** | Name of item sold |  [optional]
**barcode** | **String** |  |  [optional]
**sku** | **String** |  |  [optional]
**notes** | **String** |  |  [optional]
**notDiscountable** | **Boolean** |  |  [optional]
**price** | [**BigDecimal**](BigDecimal.md) |  | 
**cost** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**vatPerc** | [**BigDecimal**](BigDecimal.md) |  | 
**finalPrice** | [**BigDecimal**](BigDecimal.md) | Unit Price  + discount/surcharges | 
**finalNetPrice** | [**BigDecimal**](BigDecimal.md) | Final Price without VAT | 
**quantity** | [**BigDecimal**](BigDecimal.md) |  | 
**lastupdateAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**lastupdateBy** | **Integer** |  |  [optional]
**addedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**sellerId** | **Integer** |  | 
**sellerName** | **String** |  | 
**categoryId** | **Integer** |  |  [optional]
**categoryName** | **String** |  |  [optional]
**departmentId** | **Integer** |  |  [optional]
**departmentName** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**priceChanges** | [**List&lt;PricechangeSchema&gt;**](PricechangeSchema.md) |  |  [optional]
