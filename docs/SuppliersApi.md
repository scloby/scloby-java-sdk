# SuppliersApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**suppliersGet**](SuppliersApi.md#suppliersGet) | **GET** /suppliers | GET All suppliers
[**suppliersIdDelete**](SuppliersApi.md#suppliersIdDelete) | **DELETE** /suppliers/{id} | Delete existing supplier
[**suppliersIdPut**](SuppliersApi.md#suppliersIdPut) | **PUT** /suppliers/{id} | Edit existing supplier
[**suppliersPost**](SuppliersApi.md#suppliersPost) | **POST** /suppliers | Add a new supplier

<a name="suppliersGet"></a>
# **suppliersGet**
> Supplier suppliersGet(pagination, perPage, page)

GET All suppliers

Returns a Json with data about all suppliers of selected shop.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SuppliersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

SuppliersApi apiInstance = new SuppliersApi();
Boolean pagination = true; // Boolean | Pagination parameter
Integer perPage = 56; // Integer | Results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    Supplier result = apiInstance.suppliersGet(pagination, perPage, page);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SuppliersApi#suppliersGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional]
 **perPage** | **Integer**| Results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

[**Supplier**](Supplier.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="suppliersIdDelete"></a>
# **suppliersIdDelete**
> suppliersIdDelete(id)

Delete existing supplier

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SuppliersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

SuppliersApi apiInstance = new SuppliersApi();
String id = "id_example"; // String | id of the supplier that need to be deleted
try {
    apiInstance.suppliersIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling SuppliersApi#suppliersIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the supplier that need to be deleted |

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="suppliersIdPut"></a>
# **suppliersIdPut**
> Supplier suppliersIdPut(body, id)

Edit existing supplier

In this case you must specify the id in the URL and change the data you wanna update

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SuppliersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

SuppliersApi apiInstance = new SuppliersApi();
Supplier body = new Supplier(); // Supplier | Object data that need to be updated
String id = "id_example"; // String | id of the supplier that need to be updated
try {
    Supplier result = apiInstance.suppliersIdPut(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SuppliersApi#suppliersIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Supplier**](Supplier.md)| Object data that need to be updated |
 **id** | **String**| id of the supplier that need to be updated |

### Return type

[**Supplier**](Supplier.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="suppliersPost"></a>
# **suppliersPost**
> Supplier suppliersPost(body)

Add a new supplier

Returns a Json with the data of the new supplier

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SuppliersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

SuppliersApi apiInstance = new SuppliersApi();
Supplier body = new Supplier(); // Supplier | Supplier object that needs to be added to the selected shop.
try {
    Supplier result = apiInstance.suppliersPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SuppliersApi#suppliersPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Supplier**](Supplier.md)| Supplier object that needs to be added to the selected shop. |

### Return type

[**Supplier**](Supplier.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

