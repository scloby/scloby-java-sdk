# OrderitemSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**uuid** | **String** |  | 
**orderId** | **Integer** |  |  [optional]
**itemId** | **Integer** |  |  [optional]
**name** | **String** | Original Name of item sold |  [optional]
**orderName** | **String** | Name you want to print into kitchen tickets |  [optional]
**categoryId** | **Integer** |  |  [optional]
**categoryName** | **String** |  |  [optional]
**notes** | **String** |  |  [optional]
**halfPortion** | **Boolean** |  |  [optional]
**price** | [**BigDecimal**](BigDecimal.md) |  | 
**cost** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**netPrice** | [**BigDecimal**](BigDecimal.md) |  | 
**vatPerc** | [**BigDecimal**](BigDecimal.md) |  | 
**finalPrice** | [**BigDecimal**](BigDecimal.md) | Price with VAT (ingredients or variations price differences are included) | 
**finalNetPrice** | [**BigDecimal**](BigDecimal.md) | Price without VAT (ingredients or variations price differences are included) | 
**quantity** | **Integer** |  | 
**unit** | **String** |  |  [optional]
**exit** | **Integer** | Item exit, can be null or from 1 to 10 |  [optional]
**lastupdateAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**lastupdateBy** | **Integer** |  |  [optional]
**addedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**operatorId** | **Integer** |  | 
**operatorName** | **String** |  | 
**departmentId** | **Integer** |  |  [optional]
**departmentName** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**variations** | [**List&lt;OrderitemvariationSchema&gt;**](OrderitemvariationSchema.md) |  |  [optional]
**ingredients** | [**List&lt;OrderitemingredientSchema&gt;**](OrderitemingredientSchema.md) |  |  [optional]
