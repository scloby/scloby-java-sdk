# VatSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | 
**code** | **String** |  |  [optional]
**value** | [**BigDecimal**](BigDecimal.md) |  | 
