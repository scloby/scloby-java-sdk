# BookingtableSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**bookingId** | **Integer** |  |  [optional]
**tableId** | **String** |  |  [optional]
