# ShiftsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**bookingShiftsGet**](ShiftsApi.md#bookingShiftsGet) | **GET** /booking_shifts | GET All Shifts
[**bookingShiftsIdDelete**](ShiftsApi.md#bookingShiftsIdDelete) | **DELETE** /booking_shifts/{id} | Delete existing shift
[**bookingShiftsIdGet**](ShiftsApi.md#bookingShiftsIdGet) | **GET** /booking_shifts/{id} | Get existing Shift
[**bookingShiftsIdPut**](ShiftsApi.md#bookingShiftsIdPut) | **PUT** /booking_shifts/{id} | Edit existing shift
[**bookingShiftsPost**](ShiftsApi.md#bookingShiftsPost) | **POST** /booking_shifts | Add Shift

<a name="bookingShiftsGet"></a>
# **bookingShiftsGet**
> BookingShifts bookingShiftsGet(pagination, perPage, page)

GET All Shifts

Returns a Json with data about all Shifts of selected shop.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ShiftsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

ShiftsApi apiInstance = new ShiftsApi();
Boolean pagination = true; // Boolean | Pagination parameter
Integer perPage = 56; // Integer | Results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    BookingShifts result = apiInstance.bookingShiftsGet(pagination, perPage, page);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ShiftsApi#bookingShiftsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional]
 **perPage** | **Integer**| Results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

[**BookingShifts**](BookingShifts.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="bookingShiftsIdDelete"></a>
# **bookingShiftsIdDelete**
> bookingShiftsIdDelete(id)

Delete existing shift

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ShiftsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

ShiftsApi apiInstance = new ShiftsApi();
String id = "id_example"; // String | id of the shift that need to be deleted
try {
    apiInstance.bookingShiftsIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling ShiftsApi#bookingShiftsIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the shift that need to be deleted |

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="bookingShiftsIdGet"></a>
# **bookingShiftsIdGet**
> BookingShifts bookingShiftsIdGet(id)

Get existing Shift

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ShiftsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

ShiftsApi apiInstance = new ShiftsApi();
String id = "id_example"; // String | id of the shift
try {
    BookingShifts result = apiInstance.bookingShiftsIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ShiftsApi#bookingShiftsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the shift |

### Return type

[**BookingShifts**](BookingShifts.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="bookingShiftsIdPut"></a>
# **bookingShiftsIdPut**
> InlineResponse200 bookingShiftsIdPut(body, id)

Edit existing shift

In this case you must specify the id in the URL and change the data you wanna update

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ShiftsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

ShiftsApi apiInstance = new ShiftsApi();
BookingShifts body = new BookingShifts(); // BookingShifts | Object data that need to be updated
String id = "id_example"; // String | id of the shift that need to be updated
try {
    InlineResponse200 result = apiInstance.bookingShiftsIdPut(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ShiftsApi#bookingShiftsIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**BookingShifts**](BookingShifts.md)| Object data that need to be updated |
 **id** | **String**| id of the shift that need to be updated |

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="bookingShiftsPost"></a>
# **bookingShiftsPost**
> BookingShifts bookingShiftsPost(body)

Add Shift

Returns a Json with the data of the new shift

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ShiftsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

ShiftsApi apiInstance = new ShiftsApi();
BookingShifts body = new BookingShifts(); // BookingShifts | Shift object that needs to be added to the selected shop.
try {
    BookingShifts result = apiInstance.bookingShiftsPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ShiftsApi#bookingShiftsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**BookingShifts**](BookingShifts.md)| Shift object that needs to be added to the selected shop. |

### Return type

[**BookingShifts**](BookingShifts.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

