# CashMovements

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**date** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**type** | **String** |  | 
**description** | **String** |  |  [optional]
**paymentMethodName** | **String** |  |  [optional]
**cardCircuitName** | **String** |  |  [optional]
**amount** | [**BigDecimal**](BigDecimal.md) |  | 
**saleId** | **Integer** |  |  [optional]
**account** | **String** |  | 
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
