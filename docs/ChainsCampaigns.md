# ChainsCampaigns

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**fromDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**toDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**active** | **Boolean** |  | 
**rulesApplicationOnPrizes** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**createdbyId** | **Integer** |  |  [optional]
**updatedbyId** | **Integer** |  |  [optional]
**deletedbyId** | **Integer** |  |  [optional]
**isValid** | **Object** |  |  [optional]
**items** | [**List&lt;CampaignitemSchema&gt;**](CampaignitemSchema.md) |  |  [optional]
**rules** | [**List&lt;RuleSchema&gt;**](RuleSchema.md) |  |  [optional]
