# OrderitemvariationSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**orderItemId** | **Integer** |  |  [optional]
**name** | **String** | Name of variation, for example Size | 
**value** | **String** | Value of a variant, for example XL | 
**variationId** | **Integer** |  | 
**variationValueId** | **Integer** |  | 
**priceDifference** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
