# RawMaterialsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**rawMaterialsGet**](RawMaterialsApi.md#rawMaterialsGet) | **GET** /raw_materials | Get All Raw Materials
[**rawMaterialsIdDelete**](RawMaterialsApi.md#rawMaterialsIdDelete) | **DELETE** /raw_materials/{id} | Delete existing Raw material
[**rawMaterialsIdGet**](RawMaterialsApi.md#rawMaterialsIdGet) | **GET** /raw_materials/{id} | Get existing raw Material
[**rawMaterialsIdPut**](RawMaterialsApi.md#rawMaterialsIdPut) | **PUT** /raw_materials/{id} | Edit existing Raw material
[**rawMaterialsPost**](RawMaterialsApi.md#rawMaterialsPost) | **POST** /raw_materials | Add new Raw material

<a name="rawMaterialsGet"></a>
# **rawMaterialsGet**
> RawMaterial rawMaterialsGet(pagination, perPage, page)

Get All Raw Materials

Return a JSON with data about all Raw materials

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RawMaterialsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

RawMaterialsApi apiInstance = new RawMaterialsApi();
Boolean pagination = true; // Boolean | Pagination parameter
Integer perPage = 56; // Integer | Results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    RawMaterial result = apiInstance.rawMaterialsGet(pagination, perPage, page);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RawMaterialsApi#rawMaterialsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional]
 **perPage** | **Integer**| Results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

[**RawMaterial**](RawMaterial.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="rawMaterialsIdDelete"></a>
# **rawMaterialsIdDelete**
> rawMaterialsIdDelete(id)

Delete existing Raw material

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RawMaterialsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

RawMaterialsApi apiInstance = new RawMaterialsApi();
String id = "id_example"; // String | id of the raw material that need to be deleted
try {
    apiInstance.rawMaterialsIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling RawMaterialsApi#rawMaterialsIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the raw material that need to be deleted |

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="rawMaterialsIdGet"></a>
# **rawMaterialsIdGet**
> RawMaterial rawMaterialsIdGet(id)

Get existing raw Material

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RawMaterialsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

RawMaterialsApi apiInstance = new RawMaterialsApi();
String id = "id_example"; // String | id of the Raw material
try {
    RawMaterial result = apiInstance.rawMaterialsIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RawMaterialsApi#rawMaterialsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Raw material |

### Return type

[**RawMaterial**](RawMaterial.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="rawMaterialsIdPut"></a>
# **rawMaterialsIdPut**
> RawMaterial rawMaterialsIdPut(body, id)

Edit existing Raw material

In this case you must specify the id in the URL and change the data you wanna update

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RawMaterialsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

RawMaterialsApi apiInstance = new RawMaterialsApi();
RawMaterial body = new RawMaterial(); // RawMaterial | Object data that need to be updated
String id = "id_example"; // String | id of the raw material that need to be updated
try {
    RawMaterial result = apiInstance.rawMaterialsIdPut(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RawMaterialsApi#rawMaterialsIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RawMaterial**](RawMaterial.md)| Object data that need to be updated |
 **id** | **String**| id of the raw material that need to be updated |

### Return type

[**RawMaterial**](RawMaterial.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="rawMaterialsPost"></a>
# **rawMaterialsPost**
> RawMaterial rawMaterialsPost(body)

Add new Raw material

Returns a Json with the data of the new raw material

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RawMaterialsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

RawMaterialsApi apiInstance = new RawMaterialsApi();
RawMaterial body = new RawMaterial(); // RawMaterial | Item object that needs to be added.
try {
    RawMaterial result = apiInstance.rawMaterialsPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RawMaterialsApi#rawMaterialsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RawMaterial**](RawMaterial.md)| Item object that needs to be added. |

### Return type

[**RawMaterial**](RawMaterial.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

