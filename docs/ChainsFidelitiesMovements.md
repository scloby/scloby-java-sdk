# ChainsFidelitiesMovements

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**date** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**dbName** | **String** |  | 
**shopName** | **String** |  |  [optional]
**fidelity** | **String** |  | 
**campaignId** | **Integer** |  | 
**campaignName** | **String** |  | 
**ruleId** | **Integer** |  |  [optional]
**ruleName** | **String** |  |  [optional]
**prizeId** | **Integer** |  |  [optional]
**saleId** | **Integer** |  |  [optional]
**saleName** | **String** |  |  [optional]
**itemSku** | **String** |  |  [optional]
**itemName** | **String** |  |  [optional]
**points** | **Integer** |  | 
**notes** | **String** |  |  [optional]
