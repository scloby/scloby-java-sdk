# AllergensApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**allergensGet**](AllergensApi.md#allergensGet) | **GET** /allergens | Get All Allergens
[**allergensIdGet**](AllergensApi.md#allergensIdGet) | **GET** /allergens/{id} | Get existing Allergen

<a name="allergensGet"></a>
# **allergensGet**
> List&lt;InlineResponse2001&gt; allergensGet(pagination, perPage, page)

Get All Allergens

Returns a Json with data about all allergens. Allergens are static and cannot be changed, added or removed.  Not Paginated by default.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AllergensApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

AllergensApi apiInstance = new AllergensApi();
Boolean pagination = true; // Boolean | Pagination parameter
Integer perPage = 56; // Integer | Results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    List<InlineResponse2001> result = apiInstance.allergensGet(pagination, perPage, page);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AllergensApi#allergensGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional]
 **perPage** | **Integer**| Results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

[**List&lt;InlineResponse2001&gt;**](InlineResponse2001.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="allergensIdGet"></a>
# **allergensIdGet**
> InlineResponse2001 allergensIdGet(id)

Get existing Allergen

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AllergensApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

AllergensApi apiInstance = new AllergensApi();
String id = "id_example"; // String | id of the allergen
try {
    InlineResponse2001 result = apiInstance.allergensIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AllergensApi#allergensIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the allergen |

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

