# InlineResponse2004

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**campaignId** | **Integer** |  |  [optional]
**fidelity** | **String** |  |  [optional]
**points** | **Integer** |  |  [optional]
**lastupdateAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
