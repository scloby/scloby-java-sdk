# RawMaterial

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**name** | **String** | Raw Material name | 
**cost** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**stockType** | **String** |  |  [optional]
**autoUnload** | **Boolean** |  |  [optional]
**quantityAlert** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**unit** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**createdbyId** | **Integer** |  |  [optional]
**updatedbyId** | **Integer** |  |  [optional]
**deletedbyId** | **Integer** |  |  [optional]
**defaultSupplierId** | **Integer** |  |  [optional]
