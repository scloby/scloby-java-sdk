# LoyaltyProgramPointsMovementsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**fidelitiesMovementsGet**](LoyaltyProgramPointsMovementsApi.md#fidelitiesMovementsGet) | **GET** /fidelities_movements | Get All Fidelity Movements
[**fidelitiesMovementsIdGet**](LoyaltyProgramPointsMovementsApi.md#fidelitiesMovementsIdGet) | **GET** /fidelities_movements/{id} | Get existing Fidelity Movement
[**fidelitiesMovementsPost**](LoyaltyProgramPointsMovementsApi.md#fidelitiesMovementsPost) | **POST** /fidelities_movements | Add new Fidelity Movement

<a name="fidelitiesMovementsGet"></a>
# **fidelitiesMovementsGet**
> ChainsFidelitiesMovements fidelitiesMovementsGet(pagination, perPage, page)

Get All Fidelity Movements

Returns a Json with data about all fidelity movements of selected shop.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LoyaltyProgramPointsMovementsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

LoyaltyProgramPointsMovementsApi apiInstance = new LoyaltyProgramPointsMovementsApi();
Boolean pagination = true; // Boolean | Pagination parameter
Integer perPage = 56; // Integer | Results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    ChainsFidelitiesMovements result = apiInstance.fidelitiesMovementsGet(pagination, perPage, page);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LoyaltyProgramPointsMovementsApi#fidelitiesMovementsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional]
 **perPage** | **Integer**| Results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

[**ChainsFidelitiesMovements**](ChainsFidelitiesMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="fidelitiesMovementsIdGet"></a>
# **fidelitiesMovementsIdGet**
> ChainsFidelitiesMovements fidelitiesMovementsIdGet(id)

Get existing Fidelity Movement

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LoyaltyProgramPointsMovementsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

LoyaltyProgramPointsMovementsApi apiInstance = new LoyaltyProgramPointsMovementsApi();
String id = "id_example"; // String | id of the Fidelity Movement
try {
    ChainsFidelitiesMovements result = apiInstance.fidelitiesMovementsIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LoyaltyProgramPointsMovementsApi#fidelitiesMovementsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Fidelity Movement |

### Return type

[**ChainsFidelitiesMovements**](ChainsFidelitiesMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="fidelitiesMovementsPost"></a>
# **fidelitiesMovementsPost**
> ChainsFidelitiesMovements fidelitiesMovementsPost(body)

Add new Fidelity Movement

Returns a Json with the data of the new Fidelity Movement

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LoyaltyProgramPointsMovementsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

LoyaltyProgramPointsMovementsApi apiInstance = new LoyaltyProgramPointsMovementsApi();
ChainsFidelitiesMovements body = new ChainsFidelitiesMovements(); // ChainsFidelitiesMovements | Fidelity Movement object that needs to be added.
try {
    ChainsFidelitiesMovements result = apiInstance.fidelitiesMovementsPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LoyaltyProgramPointsMovementsApi#fidelitiesMovementsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ChainsFidelitiesMovements**](ChainsFidelitiesMovements.md)| Fidelity Movement object that needs to be added. |

### Return type

[**ChainsFidelitiesMovements**](ChainsFidelitiesMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

