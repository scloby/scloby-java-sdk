# ChainShopsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**chainShopsIdGet**](ChainShopsApi.md#chainShopsIdGet) | **GET** /chain_shops/{id} | Get existing Chain Shop

<a name="chainShopsIdGet"></a>
# **chainShopsIdGet**
> InlineResponse2005 chainShopsIdGet(id)

Get existing Chain Shop

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ChainShopsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

ChainShopsApi apiInstance = new ChainShopsApi();
String id = "id_example"; // String | id of the Shop
try {
    InlineResponse2005 result = apiInstance.chainShopsIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChainShopsApi#chainShopsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Shop |

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

