# ItemcombinationSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**itemId** | **Integer** |  |  [optional]
**price1** | [**BigDecimal**](BigDecimal.md) |  | 
**price2** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**price3** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**price4** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**price5** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**sku** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**combinationValues** | [**List&lt;VariationvalueSchema&gt;**](VariationvalueSchema.md) |  |  [optional]
**barcodes** | [**List&lt;CombinationbarcodeSchema&gt;**](CombinationbarcodeSchema.md) |  |  [optional]
