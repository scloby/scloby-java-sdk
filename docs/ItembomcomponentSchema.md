# ItembomcomponentSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**quantity** | [**BigDecimal**](BigDecimal.md) |  | 
**name** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**componentItemId** | **Integer** |  |  [optional]
**componentRawMaterialId** | **Integer** |  |  [optional]
**bomComponentId** | **Integer** |  |  [optional]
**itemId** | **Integer** |  |  [optional]
