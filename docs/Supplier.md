# Supplier

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**name** | **String** |  | 
**thumbnail** | **String** |  |  [optional]
**addressStreet** | **String** |  |  [optional]
**addressNumber** | **String** |  |  [optional]
**addressCity** | **String** |  |  [optional]
**addressZip** | **String** |  |  [optional]
**addressProv** | **String** |  |  [optional]
**phone1** | **String** |  |  [optional]
**phone2** | **String** |  |  [optional]
**fax** | **String** |  |  [optional]
**email1** | **String** |  |  [optional]
**email2** | **String** |  |  [optional]
**emailPec** | **String** |  |  [optional]
**website** | **String** |  |  [optional]
**vatCode** | **String** |  | 
**notes** | **String** |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**createdbyId** | **Integer** |  |  [optional]
**updatedbyId** | **Integer** |  |  [optional]
**deletedbyId** | **Integer** |  |  [optional]
