# TableSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**name** | **String** |  | 
**orderType** | **String** |  | 
**shape** | **String** |  | 
**xpos** | **Integer** |  | 
**ypos** | **Integer** |  | 
**width** | **Integer** |  | 
**height** | **Integer** |  | 
**covers** | **Integer** |  | 
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**roomId** | **Integer** |  |  [optional]
