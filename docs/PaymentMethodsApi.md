# PaymentMethodsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**paymentMethodsGet**](PaymentMethodsApi.md#paymentMethodsGet) | **GET** /payment_methods | Get All Payment Methods
[**paymentMethodsIdDelete**](PaymentMethodsApi.md#paymentMethodsIdDelete) | **DELETE** /payment_methods/{id} | Delete existing Vat rate
[**paymentMethodsIdGet**](PaymentMethodsApi.md#paymentMethodsIdGet) | **GET** /payment_methods/{id} | Get existing Payment Method
[**paymentMethodsIdPut**](PaymentMethodsApi.md#paymentMethodsIdPut) | **PUT** /payment_methods/{id} | Edit existing Payment Method
[**paymentMethodsPost**](PaymentMethodsApi.md#paymentMethodsPost) | **POST** /payment_methods | Add new Payment Method

<a name="paymentMethodsGet"></a>
# **paymentMethodsGet**
> PaymentMethods paymentMethodsGet(pagination, perPage, page)

Get All Payment Methods

Returns a Json with data about all payment methods of selected shop.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PaymentMethodsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

PaymentMethodsApi apiInstance = new PaymentMethodsApi();
Boolean pagination = true; // Boolean | Pagination parameter
Integer perPage = 56; // Integer | Results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    PaymentMethods result = apiInstance.paymentMethodsGet(pagination, perPage, page);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentMethodsApi#paymentMethodsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional]
 **perPage** | **Integer**| Results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

[**PaymentMethods**](PaymentMethods.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="paymentMethodsIdDelete"></a>
# **paymentMethodsIdDelete**
> paymentMethodsIdDelete(id)

Delete existing Vat rate

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PaymentMethodsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

PaymentMethodsApi apiInstance = new PaymentMethodsApi();
String id = "id_example"; // String | id of the Payment method that need to be deleted
try {
    apiInstance.paymentMethodsIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentMethodsApi#paymentMethodsIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Payment method that need to be deleted |

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="paymentMethodsIdGet"></a>
# **paymentMethodsIdGet**
> PaymentMethods paymentMethodsIdGet(id)

Get existing Payment Method

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PaymentMethodsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

PaymentMethodsApi apiInstance = new PaymentMethodsApi();
String id = "id_example"; // String | id of the Payment Method
try {
    PaymentMethods result = apiInstance.paymentMethodsIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentMethodsApi#paymentMethodsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Payment Method |

### Return type

[**PaymentMethods**](PaymentMethods.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="paymentMethodsIdPut"></a>
# **paymentMethodsIdPut**
> PaymentMethods paymentMethodsIdPut(body, id)

Edit existing Payment Method

In this case you must specify the id in the URL and change the data you wanna update

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PaymentMethodsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

PaymentMethodsApi apiInstance = new PaymentMethodsApi();
PaymentMethods body = new PaymentMethods(); // PaymentMethods | Object data that need to be updated
String id = "id_example"; // String | id of the Payment Method that need to be updated
try {
    PaymentMethods result = apiInstance.paymentMethodsIdPut(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentMethodsApi#paymentMethodsIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PaymentMethods**](PaymentMethods.md)| Object data that need to be updated |
 **id** | **String**| id of the Payment Method that need to be updated |

### Return type

[**PaymentMethods**](PaymentMethods.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="paymentMethodsPost"></a>
# **paymentMethodsPost**
> PaymentMethods paymentMethodsPost(body)

Add new Payment Method

Returns a Json with the data of the new Payment Method

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PaymentMethodsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

PaymentMethodsApi apiInstance = new PaymentMethodsApi();
PaymentMethods body = new PaymentMethods(); // PaymentMethods | Payment Method object that needs to be added.
try {
    PaymentMethods result = apiInstance.paymentMethodsPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentMethodsApi#paymentMethodsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PaymentMethods**](PaymentMethods.md)| Payment Method object that needs to be added. |

### Return type

[**PaymentMethods**](PaymentMethods.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

