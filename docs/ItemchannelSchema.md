# ItemchannelSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**channelId** | **String** |  | 
**defaultPricelist** | **Integer** |  |  [optional]
**categoryId** | **Integer** |  |  [optional]
**itemId** | **Integer** |  |  [optional]
