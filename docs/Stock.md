# Stock

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**itemId** | **Integer** |  |  [optional]
**combinationId** | **Integer** |  |  [optional]
**rawMaterialId** | **Integer** |  |  [optional]
**itemOnSale** | **Boolean** |  |  [optional]
**stockQuantity** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**available** | **String** |  |  [optional]
**unit** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**barcode** | **String** |  |  [optional]
**code** | **String** |  |  [optional]
**optionsValues** | **String** |  |  [optional]
**combination** | **String** |  |  [optional]
**lastupdateAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
