# WarehouseStockMovementsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stockMovementsGet**](WarehouseStockMovementsApi.md#stockMovementsGet) | **GET** /stock_movements | Get All Stock Movements
[**stockMovementsIdDelete**](WarehouseStockMovementsApi.md#stockMovementsIdDelete) | **DELETE** /stock_movements/{id} | Delete existing Stock Movement
[**stockMovementsIdGet**](WarehouseStockMovementsApi.md#stockMovementsIdGet) | **GET** /stock_movements/{id} | Get existing stock movement
[**stockMovementsIdPut**](WarehouseStockMovementsApi.md#stockMovementsIdPut) | **PUT** /stock_movements/{id} | Edit existing Stock Movement
[**stockMovementsPost**](WarehouseStockMovementsApi.md#stockMovementsPost) | **POST** /stock_movements | Add new Stock Movements

<a name="stockMovementsGet"></a>
# **stockMovementsGet**
> StockMovements stockMovementsGet(pagination, perPage, page)

Get All Stock Movements

Returns a Json with data about all stock movements of selected shop.  Paginated by default (default per_page &#x3D; 1000)

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WarehouseStockMovementsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

WarehouseStockMovementsApi apiInstance = new WarehouseStockMovementsApi();
Boolean pagination = true; // Boolean | Pagination parameter
Integer perPage = 56; // Integer | Results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    StockMovements result = apiInstance.stockMovementsGet(pagination, perPage, page);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WarehouseStockMovementsApi#stockMovementsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional]
 **perPage** | **Integer**| Results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

[**StockMovements**](StockMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="stockMovementsIdDelete"></a>
# **stockMovementsIdDelete**
> stockMovementsIdDelete(id)

Delete existing Stock Movement

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WarehouseStockMovementsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

WarehouseStockMovementsApi apiInstance = new WarehouseStockMovementsApi();
String id = "id_example"; // String | id of the Stock Movement that need to be deleted
try {
    apiInstance.stockMovementsIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling WarehouseStockMovementsApi#stockMovementsIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Stock Movement that need to be deleted |

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="stockMovementsIdGet"></a>
# **stockMovementsIdGet**
> StockMovements stockMovementsIdGet(id)

Get existing stock movement

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WarehouseStockMovementsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

WarehouseStockMovementsApi apiInstance = new WarehouseStockMovementsApi();
String id = "id_example"; // String | id of the stock movement
try {
    StockMovements result = apiInstance.stockMovementsIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WarehouseStockMovementsApi#stockMovementsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the stock movement |

### Return type

[**StockMovements**](StockMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="stockMovementsIdPut"></a>
# **stockMovementsIdPut**
> StockMovements stockMovementsIdPut(body, id)

Edit existing Stock Movement

In this case you must specify the id in the URL and change the data you wanna update

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WarehouseStockMovementsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

WarehouseStockMovementsApi apiInstance = new WarehouseStockMovementsApi();
StockMovements body = new StockMovements(); // StockMovements | Object data that need to be updated
String id = "id_example"; // String | id of the stock movement that need to be updated
try {
    StockMovements result = apiInstance.stockMovementsIdPut(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WarehouseStockMovementsApi#stockMovementsIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**StockMovements**](StockMovements.md)| Object data that need to be updated |
 **id** | **String**| id of the stock movement that need to be updated |

### Return type

[**StockMovements**](StockMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="stockMovementsPost"></a>
# **stockMovementsPost**
> StockMovements stockMovementsPost(body)

Add new Stock Movements

Returns a Json with the data of the new Stock Movements.  **Stock movements type**  Each Stock Movement can be one of these three types: - load (increase quantity) - unload (decrease quantity) - setup (set quantity)  and can be referred to: - Item (item_id is not null and raw_material is null) - Raw Material (raw_material_id is not null and item_id is null)   **Stock movements causes**  Load causes: - buy - refund - gift - replacement - production   Unload causes: - sale - gift - move - damage - theft - replacement - waste          

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WarehouseStockMovementsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

WarehouseStockMovementsApi apiInstance = new WarehouseStockMovementsApi();
StockMovements body = new StockMovements(); // StockMovements | Item object that needs to be added.
try {
    StockMovements result = apiInstance.stockMovementsPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WarehouseStockMovementsApi#stockMovementsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**StockMovements**](StockMovements.md)| Item object that needs to be added. |

### Return type

[**StockMovements**](StockMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

