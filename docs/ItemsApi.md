# ItemsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**itemsGet**](ItemsApi.md#itemsGet) | **GET** /items | GET All items
[**itemsIdDelete**](ItemsApi.md#itemsIdDelete) | **DELETE** /items/{id} | Delete existing Item
[**itemsIdGet**](ItemsApi.md#itemsIdGet) | **GET** /items/{id} | Get existing item
[**itemsIdPut**](ItemsApi.md#itemsIdPut) | **PUT** /items/{id} | Edit existing item
[**itemsPost**](ItemsApi.md#itemsPost) | **POST** /items | Add new Item

<a name="itemsGet"></a>
# **itemsGet**
> Items itemsGet(pagination, perPage, page)

GET All items

Returns a Json with data about all items of selected shop.  Paginated by default (per_page&#x3D;1000)

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ItemsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

ItemsApi apiInstance = new ItemsApi();
Boolean pagination = true; // Boolean | Pagination param
Integer perPage = 56; // Integer | results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    Items result = apiInstance.itemsGet(pagination, perPage, page);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ItemsApi#itemsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination param | [optional]
 **perPage** | **Integer**| results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

[**Items**](Items.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="itemsIdDelete"></a>
# **itemsIdDelete**
> itemsIdDelete(id)

Delete existing Item

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ItemsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

ItemsApi apiInstance = new ItemsApi();
String id = "id_example"; // String | id of the item that need to be deleted
try {
    apiInstance.itemsIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling ItemsApi#itemsIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the item that need to be deleted |

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="itemsIdGet"></a>
# **itemsIdGet**
> Items itemsIdGet(id)

Get existing item

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ItemsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

ItemsApi apiInstance = new ItemsApi();
String id = "id_example"; // String | id of the item
try {
    Items result = apiInstance.itemsIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ItemsApi#itemsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the item |

### Return type

[**Items**](Items.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="itemsIdPut"></a>
# **itemsIdPut**
> Items itemsIdPut(body, id)

Edit existing item

In this case you must specify the id in the URL and change the data you wanna update.  As every scloby resource, you can do a PUT request. First of all you have to save the original content of the resource (simply doing a GET request), edit what do you want, and resend it in a PUT request.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ItemsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

ItemsApi apiInstance = new ItemsApi();
Items body = new Items(); // Items | Object data that need to be updated
String id = "id_example"; // String | id of the item that need to be updated
try {
    Items result = apiInstance.itemsIdPut(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ItemsApi#itemsIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Items**](Items.md)| Object data that need to be updated |
 **id** | **String**| id of the item that need to be updated |

### Return type

[**Items**](Items.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="itemsPost"></a>
# **itemsPost**
> Items itemsPost(body)

Add new Item

You must know first the **department id** and the **category id**. The category id can be not set, in this case its value will be null, and the item will be shown in the section No category of the app. If the item is a restaurant item you must know if he has **allergens** and insert the ids in the **allergents** node of json. If this item has **ingredients**, you can specify the **components** ids.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ItemsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

ItemsApi apiInstance = new ItemsApi();
Items body = new Items(); // Items | Item object that needs to be added.
try {
    Items result = apiInstance.itemsPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ItemsApi#itemsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Items**](Items.md)| Item object that needs to be added. |

### Return type

[**Items**](Items.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

