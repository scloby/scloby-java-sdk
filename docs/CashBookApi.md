# CashBookApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cashMovementsGet**](CashBookApi.md#cashMovementsGet) | **GET** /cash_movements | Get All Cash Movements
[**cashMovementsIdDelete**](CashBookApi.md#cashMovementsIdDelete) | **DELETE** /cash_movements/{id} | Delete existing Cash Movement
[**cashMovementsIdGet**](CashBookApi.md#cashMovementsIdGet) | **GET** /cash_movements/{id} | Get existing Cash Movement
[**cashMovementsIdPut**](CashBookApi.md#cashMovementsIdPut) | **PUT** /cash_movements/{id} | Edit existing Cash Movement
[**cashMovementsPost**](CashBookApi.md#cashMovementsPost) | **POST** /cash_movements | Add new Cash movement

<a name="cashMovementsGet"></a>
# **cashMovementsGet**
> CashMovements cashMovementsGet(pagination, perPage, page)

Get All Cash Movements

Returns a Json with data about all cash movements of selected shop.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CashBookApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

CashBookApi apiInstance = new CashBookApi();
Boolean pagination = true; // Boolean | Pagination parameter
Integer perPage = 56; // Integer | Results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    CashMovements result = apiInstance.cashMovementsGet(pagination, perPage, page);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CashBookApi#cashMovementsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional]
 **perPage** | **Integer**| Results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

[**CashMovements**](CashMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="cashMovementsIdDelete"></a>
# **cashMovementsIdDelete**
> cashMovementsIdDelete(id)

Delete existing Cash Movement

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CashBookApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

CashBookApi apiInstance = new CashBookApi();
String id = "id_example"; // String | id of the Cash Movement that need to be deleted
try {
    apiInstance.cashMovementsIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling CashBookApi#cashMovementsIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Cash Movement that need to be deleted |

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="cashMovementsIdGet"></a>
# **cashMovementsIdGet**
> CashMovements cashMovementsIdGet(id)

Get existing Cash Movement

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CashBookApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

CashBookApi apiInstance = new CashBookApi();
String id = "id_example"; // String | id of the Cash Movement
try {
    CashMovements result = apiInstance.cashMovementsIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CashBookApi#cashMovementsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Cash Movement |

### Return type

[**CashMovements**](CashMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="cashMovementsIdPut"></a>
# **cashMovementsIdPut**
> CashMovements cashMovementsIdPut(body, id)

Edit existing Cash Movement

In this case you must specify the id in the URL and change the data you wanna update

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CashBookApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

CashBookApi apiInstance = new CashBookApi();
CashMovements body = new CashMovements(); // CashMovements | Object data that need to be updated
String id = "id_example"; // String | id of the Cash Movement that need to be updated
try {
    CashMovements result = apiInstance.cashMovementsIdPut(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CashBookApi#cashMovementsIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CashMovements**](CashMovements.md)| Object data that need to be updated |
 **id** | **String**| id of the Cash Movement that need to be updated |

### Return type

[**CashMovements**](CashMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="cashMovementsPost"></a>
# **cashMovementsPost**
> CashMovements cashMovementsPost(body)

Add new Cash movement

Returns a Json with the data of the new Cash movement

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CashBookApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

CashBookApi apiInstance = new CashBookApi();
CashMovements body = new CashMovements(); // CashMovements | Cash movement object that needs to be added.
try {
    CashMovements result = apiInstance.cashMovementsPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CashBookApi#cashMovementsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CashMovements**](CashMovements.md)| Cash movement object that needs to be added. |

### Return type

[**CashMovements**](CashMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

