# InlineResponse200

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**weekdaysPeriod** | **List&lt;String&gt;** |  |  [optional]
**id** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**startTime** | **String** |  |  [optional]
**endTime** | **String** |  |  [optional]
**startPeriod** | **String** |  |  [optional]
**endPeriod** | **String** |  |  [optional]
**defaultDuration** | **Integer** |  |  [optional]
**defaultPricelist** | **Integer** |  |  [optional]
**instoreSeatsLimit** | **Integer** |  |  [optional]
**onlineSeatsLimit** | **Integer** |  |  [optional]
**roomRestrictions** | **List&lt;String&gt;** |  |  [optional]
