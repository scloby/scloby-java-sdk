# PrepaidCreditMovementsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**prepaidCustomersGet**](PrepaidCreditMovementsApi.md#prepaidCustomersGet) | **GET** /prepaid_customers | Get list of Prepaid Customers
[**prepaidMovementsIdGet**](PrepaidCreditMovementsApi.md#prepaidMovementsIdGet) | **GET** /prepaid_movements/{id} | Get existing Prepaid Movement
[**prepaidMovementsPost**](PrepaidCreditMovementsApi.md#prepaidMovementsPost) | **POST** /prepaid_movements | Add new Prepaid Movement

<a name="prepaidCustomersGet"></a>
# **prepaidCustomersGet**
> prepaidCustomersGet(pagination, perPage, page)

Get list of Prepaid Customers

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PrepaidCreditMovementsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

PrepaidCreditMovementsApi apiInstance = new PrepaidCreditMovementsApi();
Boolean pagination = true; // Boolean | Pagination parameter
Integer perPage = 56; // Integer | Results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    apiInstance.prepaidCustomersGet(pagination, perPage, page);
} catch (ApiException e) {
    System.err.println("Exception when calling PrepaidCreditMovementsApi#prepaidCustomersGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional]
 **perPage** | **Integer**| Results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="prepaidMovementsIdGet"></a>
# **prepaidMovementsIdGet**
> ChainPrepaidMovements prepaidMovementsIdGet(id)

Get existing Prepaid Movement

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PrepaidCreditMovementsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

PrepaidCreditMovementsApi apiInstance = new PrepaidCreditMovementsApi();
String id = "id_example"; // String | id of the Prepaid Movement
try {
    ChainPrepaidMovements result = apiInstance.prepaidMovementsIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PrepaidCreditMovementsApi#prepaidMovementsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Prepaid Movement |

### Return type

[**ChainPrepaidMovements**](ChainPrepaidMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="prepaidMovementsPost"></a>
# **prepaidMovementsPost**
> ChainPrepaidMovements prepaidMovementsPost(body, pagination, perPage, page)

Add new Prepaid Movement

Returns a Json with the data of the new Prepaid Movement

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PrepaidCreditMovementsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

PrepaidCreditMovementsApi apiInstance = new PrepaidCreditMovementsApi();
ChainPrepaidMovements body = new ChainPrepaidMovements(); // ChainPrepaidMovements | Prepaid Movement object that needs to be added.
Boolean pagination = true; // Boolean | Pagination parameter
Integer perPage = 56; // Integer | Results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    ChainPrepaidMovements result = apiInstance.prepaidMovementsPost(body, pagination, perPage, page);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PrepaidCreditMovementsApi#prepaidMovementsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ChainPrepaidMovements**](ChainPrepaidMovements.md)| Prepaid Movement object that needs to be added. |
 **pagination** | **Boolean**| Pagination parameter | [optional]
 **perPage** | **Integer**| Results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

[**ChainPrepaidMovements**](ChainPrepaidMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

