# BookingsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**bookingsGet**](BookingsApi.md#bookingsGet) | **GET** /bookings | GET All bookings
[**bookingsIdDelete**](BookingsApi.md#bookingsIdDelete) | **DELETE** /bookings/{id} | Delete existing booking
[**bookingsIdGet**](BookingsApi.md#bookingsIdGet) | **GET** /bookings/{id} | Get existing booking
[**bookingsIdPut**](BookingsApi.md#bookingsIdPut) | **PUT** /bookings/{id} | Edit existing booking
[**bookingsPost**](BookingsApi.md#bookingsPost) | **POST** /bookings | Add booking

<a name="bookingsGet"></a>
# **bookingsGet**
> Bookings bookingsGet(pagination, perPage, page)

GET All bookings

Returns a Json with data about all bookings of selected shop.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.BookingsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

BookingsApi apiInstance = new BookingsApi();
Boolean pagination = true; // Boolean | Pagination parameter
Integer perPage = 56; // Integer | Results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    Bookings result = apiInstance.bookingsGet(pagination, perPage, page);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BookingsApi#bookingsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional]
 **perPage** | **Integer**| Results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

[**Bookings**](Bookings.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="bookingsIdDelete"></a>
# **bookingsIdDelete**
> bookingsIdDelete(id)

Delete existing booking

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.BookingsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

BookingsApi apiInstance = new BookingsApi();
String id = "id_example"; // String | id of the booking that need to be deleted
try {
    apiInstance.bookingsIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling BookingsApi#bookingsIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the booking that need to be deleted |

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="bookingsIdGet"></a>
# **bookingsIdGet**
> Bookings bookingsIdGet(id)

Get existing booking

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.BookingsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

BookingsApi apiInstance = new BookingsApi();
String id = "id_example"; // String | id of the booking
try {
    Bookings result = apiInstance.bookingsIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BookingsApi#bookingsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the booking |

### Return type

[**Bookings**](Bookings.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="bookingsIdPut"></a>
# **bookingsIdPut**
> Bookings bookingsIdPut(body, id)

Edit existing booking

In this case you must specify the id in the URL and change the data you wanna update

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.BookingsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

BookingsApi apiInstance = new BookingsApi();
Bookings body = new Bookings(); // Bookings | Object data that need to be updated
String id = "id_example"; // String | id of the booking that need to be updated
try {
    Bookings result = apiInstance.bookingsIdPut(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BookingsApi#bookingsIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Bookings**](Bookings.md)| Object data that need to be updated |
 **id** | **String**| id of the booking that need to be updated |

### Return type

[**Bookings**](Bookings.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="bookingsPost"></a>
# **bookingsPost**
> Bookings bookingsPost(body)

Add booking

Returns a Json with the data of the new reservation

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.BookingsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

BookingsApi apiInstance = new BookingsApi();
Bookings body = new Bookings(); // Bookings | Booking object that needs to be added to the selected shop.
try {
    Bookings result = apiInstance.bookingsPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BookingsApi#bookingsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Bookings**](Bookings.md)| Booking object that needs to be added to the selected shop. |

### Return type

[**Bookings**](Bookings.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

