# SaledocumentSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**saleId** | **Integer** |  |  [optional]
**sequentialNumber** | **Integer** |  |  [optional]
**printerSerial** | **String** |  |  [optional]
**printerId** | **Integer** |  |  [optional]
**printerName** | **String** |  |  [optional]
**date** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**documentType** | **String** |  | 
**documentUrl** | **String** |  |  [optional]
**documentContent** | **String** |  |  [optional]
**metadata** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**meta** | **Object** |  |  [optional]
