# PaymentMethods

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | 
**name** | **String** |  | 
**paymentMethodTypeId** | **Integer** |  | 
**enableSum** | **Boolean** |  | 
**unclaimed** | **Boolean** |  | 
**hidden** | **Boolean** |  |  [optional]
**bundleName** | **String** |  |  [optional]
**schemaName** | **String** |  |  [optional]
