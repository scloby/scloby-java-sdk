# ChainsPrizes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**campaignId** | **Integer** |  | 
**name** | **String** |  | 
**points** | **Integer** |  | 
**type** | **String** |  | 
**discountAmount** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**itemSku** | **String** |  |  [optional]
**validFrom** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**validTo** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**createdbyId** | **Integer** |  |  [optional]
**updatedbyId** | **Integer** |  |  [optional]
**deletedbyId** | **Integer** |  |  [optional]
