# VariationvalueSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**variationId** | **Integer** |  |  [optional]
**index** | **Integer** |  |  [optional]
**value** | **String** |  |  [optional]
**defaultValue** | **Boolean** |  |  [optional]
**code** | **String** |  |  [optional]
**price** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**priceDifference** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**imageUrl** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
