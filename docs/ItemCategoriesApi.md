# ItemCategoriesApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**categoriesGet**](ItemCategoriesApi.md#categoriesGet) | **GET** /categories | Get All Categories
[**categoriesIdDelete**](ItemCategoriesApi.md#categoriesIdDelete) | **DELETE** /categories/{id} | Delete existing Category
[**categoriesIdGet**](ItemCategoriesApi.md#categoriesIdGet) | **GET** /categories/{id} | Get existing category
[**categoriesIdPut**](ItemCategoriesApi.md#categoriesIdPut) | **PUT** /categories/{id} | Edit existing Category
[**categoriesPost**](ItemCategoriesApi.md#categoriesPost) | **POST** /categories | Add new Category

<a name="categoriesGet"></a>
# **categoriesGet**
> Categories categoriesGet(pagination, perPage, page)

Get All Categories

Return a JSON of all categories

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ItemCategoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

ItemCategoriesApi apiInstance = new ItemCategoriesApi();
Boolean pagination = true; // Boolean | Pagination parameter
Integer perPage = 56; // Integer | Results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    Categories result = apiInstance.categoriesGet(pagination, perPage, page);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ItemCategoriesApi#categoriesGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional]
 **perPage** | **Integer**| Results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

[**Categories**](Categories.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="categoriesIdDelete"></a>
# **categoriesIdDelete**
> categoriesIdDelete(id)

Delete existing Category

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ItemCategoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

ItemCategoriesApi apiInstance = new ItemCategoriesApi();
String id = "id_example"; // String | id of the Category that need to be deleted
try {
    apiInstance.categoriesIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling ItemCategoriesApi#categoriesIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Category that need to be deleted |

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="categoriesIdGet"></a>
# **categoriesIdGet**
> Categories categoriesIdGet(id)

Get existing category

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ItemCategoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

ItemCategoriesApi apiInstance = new ItemCategoriesApi();
String id = "id_example"; // String | id of the category
try {
    Categories result = apiInstance.categoriesIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ItemCategoriesApi#categoriesIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the category |

### Return type

[**Categories**](Categories.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="categoriesIdPut"></a>
# **categoriesIdPut**
> Categories categoriesIdPut(body, id)

Edit existing Category

In this case you must specify the id in the URL and change the data you wanna update

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ItemCategoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

ItemCategoriesApi apiInstance = new ItemCategoriesApi();
Categories body = new Categories(); // Categories | Object data that need to be updated
String id = "id_example"; // String | id of the Category that need to be updated
try {
    Categories result = apiInstance.categoriesIdPut(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ItemCategoriesApi#categoriesIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Categories**](Categories.md)| Object data that need to be updated |
 **id** | **String**| id of the Category that need to be updated |

### Return type

[**Categories**](Categories.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="categoriesPost"></a>
# **categoriesPost**
> Categories categoriesPost(body)

Add new Category

Returns a Json with the data of the new Category

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ItemCategoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

ItemCategoriesApi apiInstance = new ItemCategoriesApi();
Categories body = new Categories(); // Categories | Category object that needs to be added.
try {
    Categories result = apiInstance.categoriesPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ItemCategoriesApi#categoriesPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Categories**](Categories.md)| Category object that needs to be added. |

### Return type

[**Categories**](Categories.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

