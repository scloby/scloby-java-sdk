# Session

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**username** | **String** |  |  [optional]
**email** | **String** |  |  [optional]
**phone** | **String** |  |  [optional]
**shop** | [**SessionShop**](SessionShop.md) |  |  [optional]
