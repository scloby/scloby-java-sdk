# InlineResponse2012

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**campaignId** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**points** | **Integer** |  |  [optional]
**type** | **String** |  |  [optional]
**discountAmount** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**itemSku** | **String** |  |  [optional]
**validFrom** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**validTo** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
