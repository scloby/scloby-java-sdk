# LoyaltyProgramRulesApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**rulesGet**](LoyaltyProgramRulesApi.md#rulesGet) | **GET** /rules | Get All Rules
[**rulesIdDelete**](LoyaltyProgramRulesApi.md#rulesIdDelete) | **DELETE** /rules/{id} | Delete existing Rule
[**rulesIdGet**](LoyaltyProgramRulesApi.md#rulesIdGet) | **GET** /rules/{id} | Get existing Rule
[**rulesIdPut**](LoyaltyProgramRulesApi.md#rulesIdPut) | **PUT** /rules/{id} | Edit existing Rule
[**rulesPost**](LoyaltyProgramRulesApi.md#rulesPost) | **POST** /rules | Add new Rule

<a name="rulesGet"></a>
# **rulesGet**
> ChainsRules rulesGet(pagination, perPage, page)

Get All Rules

Returns a Json with data about all rules of selected shop.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LoyaltyProgramRulesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

LoyaltyProgramRulesApi apiInstance = new LoyaltyProgramRulesApi();
Boolean pagination = true; // Boolean | Pagination parameter
Integer perPage = 56; // Integer | Results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    ChainsRules result = apiInstance.rulesGet(pagination, perPage, page);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LoyaltyProgramRulesApi#rulesGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional]
 **perPage** | **Integer**| Results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

[**ChainsRules**](ChainsRules.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="rulesIdDelete"></a>
# **rulesIdDelete**
> rulesIdDelete(id)

Delete existing Rule

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LoyaltyProgramRulesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

LoyaltyProgramRulesApi apiInstance = new LoyaltyProgramRulesApi();
String id = "id_example"; // String | id of the Rule that need to be deleted
try {
    apiInstance.rulesIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling LoyaltyProgramRulesApi#rulesIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Rule that need to be deleted |

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="rulesIdGet"></a>
# **rulesIdGet**
> ChainsRules rulesIdGet(id)

Get existing Rule

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LoyaltyProgramRulesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

LoyaltyProgramRulesApi apiInstance = new LoyaltyProgramRulesApi();
String id = "id_example"; // String | id of the Rule
try {
    ChainsRules result = apiInstance.rulesIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LoyaltyProgramRulesApi#rulesIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Rule |

### Return type

[**ChainsRules**](ChainsRules.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="rulesIdPut"></a>
# **rulesIdPut**
> ChainsRules rulesIdPut(body, id)

Edit existing Rule

In this case you must specify the id in the URL and change the data you wanna update

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LoyaltyProgramRulesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

LoyaltyProgramRulesApi apiInstance = new LoyaltyProgramRulesApi();
ChainsRules body = new ChainsRules(); // ChainsRules | Object data that need to be updated
String id = "id_example"; // String | id of the Rule that need to be updated
try {
    ChainsRules result = apiInstance.rulesIdPut(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LoyaltyProgramRulesApi#rulesIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ChainsRules**](ChainsRules.md)| Object data that need to be updated |
 **id** | **String**| id of the Rule that need to be updated |

### Return type

[**ChainsRules**](ChainsRules.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="rulesPost"></a>
# **rulesPost**
> ChainsRules rulesPost(body)

Add new Rule

Returns a Json with the data of the new Rule

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LoyaltyProgramRulesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

LoyaltyProgramRulesApi apiInstance = new LoyaltyProgramRulesApi();
ChainsRules body = new ChainsRules(); // ChainsRules | Rule object that needs to be added.
try {
    ChainsRules result = apiInstance.rulesPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LoyaltyProgramRulesApi#rulesPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ChainsRules**](ChainsRules.md)| Rule object that needs to be added. |

### Return type

[**ChainsRules**](ChainsRules.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

