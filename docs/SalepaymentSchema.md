# SalepaymentSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**saleId** | **Integer** |  |  [optional]
**paymentMethodId** | **Integer** |  | 
**paymentMethodName** | **String** |  | 
**paymentMethodTypeId** | **Integer** |  | 
**paymentMethodTypeName** | **String** |  | 
**unclaimed** | **Boolean** |  |  [optional]
**amount** | [**BigDecimal**](BigDecimal.md) |  | 
**paid** | **Boolean** |  |  [optional]
**date** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**acquirerName** | **String** |  |  [optional]
**acquirerId** | **Integer** |  |  [optional]
**paymentData** | **String** |  |  [optional]
**trx** | **String** |  |  [optional]
**calltrx** | **String** |  |  [optional]
**cardCircuitId** | **Integer** |  |  [optional]
**cardCircuitName** | **String** |  |  [optional]
**ticketCircuit** | **String** |  |  [optional]
**ticketName** | **String** |  |  [optional]
**ticketId** | **Integer** |  |  [optional]
**code** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
