# EInvoiceApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**salesEInvoiceCheckPost**](EInvoiceApi.md#salesEInvoiceCheckPost) | **POST** /sales/e_invoice/check | Check e-invoice status
[**sendEInvoiceIdGet**](EInvoiceApi.md#sendEInvoiceIdGet) | **GET** /send_e_invoice/{id} | Send e-invoice

<a name="salesEInvoiceCheckPost"></a>
# **salesEInvoiceCheckPost**
> InlineResponse2002 salesEInvoiceCheckPost(body)

Check e-invoice status

Return a JSON of the sale

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EInvoiceApi;


EInvoiceApi apiInstance = new EInvoiceApi();
Sales body = new Sales(); // Sales | Object of the Sale
try {
    InlineResponse2002 result = apiInstance.salesEInvoiceCheckPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EInvoiceApi#salesEInvoiceCheckPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Sales**](Sales.md)| Object of the Sale |

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="sendEInvoiceIdGet"></a>
# **sendEInvoiceIdGet**
> Sales sendEInvoiceIdGet(id)

Send e-invoice

In this case you must specify the id of the sale in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.EInvoiceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

EInvoiceApi apiInstance = new EInvoiceApi();
String id = "id_example"; // String | id of the sale
try {
    Sales result = apiInstance.sendEInvoiceIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EInvoiceApi#sendEInvoiceIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the sale |

### Return type

[**Sales**](Sales.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

