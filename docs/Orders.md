# Orders

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**uuid** | **String** |  |  [optional]
**orderParentId** | **Integer** |  |  [optional]
**orderParentUuid** | **String** |  |  [optional]
**name** | **String** |  | 
**type** | **String** | Order type, can be normal or take_away | 
**paid** | **Boolean** |  |  [optional]
**externalId** | **String** |  |  [optional]
**channel** | **String** |  |  [optional]
**notes** | **String** |  |  [optional]
**status** | **String** | Order status, can be open, checkout, closed or missed | 
**amount** | [**BigDecimal**](BigDecimal.md) | Sum of final_price of all order_items |  [optional]
**netAmount** | [**BigDecimal**](BigDecimal.md) | Sum of final_net_price of all order_items |  [optional]
**roomId** | **Integer** |  |  [optional]
**roomName** | **String** |  |  [optional]
**tableId** | **Integer** |  |  [optional]
**tableName** | **String** |  |  [optional]
**operatorId** | **Integer** |  | 
**operatorName** | **String** |  | 
**lastupdateAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**lastupdateBy** | **Integer** |  |  [optional]
**orderNumber** | **Integer** |  |  [optional]
**covers** | **Integer** |  |  [optional]
**openAt** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**checkoutAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**closedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deliverAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**lastSentBy** | **Integer** |  |  [optional]
**lastSentAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**previous** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**createdbyId** | **Integer** |  |  [optional]
**updatedbyId** | **Integer** |  |  [optional]
**deletedbyId** | **Integer** |  |  [optional]
**previousOrder** | **Object** |  |  [optional]
**orderItems** | [**List&lt;OrderitemSchema&gt;**](OrderitemSchema.md) |  |  [optional]
**orderCustomer** | [**OrdercustomerSchema**](OrdercustomerSchema.md) |  |  [optional]
