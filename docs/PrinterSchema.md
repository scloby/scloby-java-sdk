# PrinterSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**name** | **String** |  | 
**ipAddress** | **String** |  |  [optional]
**subnetMask** | **String** |  |  [optional]
**gateway** | **String** |  |  [optional]
**macAddressBt** | **String** |  |  [optional]
**ssl** | **Boolean** |  |  [optional]
**port** | **Integer** |  |  [optional]
**driver** | **String** | Driver type, can be escpos, rch, epson | 
**connectionType** | **String** | Connection type, can be &#x27;ws&#x27; (webservice) or &#x27;bt&#x27; (bluetooth) |  [optional]
**type** | **String** | printer type, can be fiscal or nonfiscal | 
**printFiscalReceipt** | **Boolean** | true if can print fiscal receipts, false otherwise |  [optional]
**printReceipt** | **Boolean** | true if can print receipts, false otherwise |  [optional]
**printInvoice** | **Boolean** | true if can print invoices, false otherwise |  [optional]
**printReceiptInvoice** | **Boolean** | true if can print invoices based on a previously printed receipt, false otherwise |  [optional]
**printSummaryInvoice** | **Boolean** | true if can print summary invoices |  [optional]
**printSummaryENrc** | **Boolean** | true if can print summary non claimed e-invoices |  [optional]
**printSummaryERc** | **Boolean** | true if can print summary claimed e-invoices |  [optional]
**printShippingInvoice** | **Boolean** | true if can print shipping invoices |  [optional]
**printEInvoice** | **Boolean** | true if can print e-invoices |  [optional]
**printGenericReceipt** | **Boolean** | true if can print generic receipts |  [optional]
**printGenericInvoice** | **Boolean** | true if can print generic invoices |  [optional]
**printGenericDocument** | **Boolean** | true if can print generic documents |  [optional]
**receiptFormat** | **String** | receipt format, for ex. 10 or 20 lines |  [optional]
**columns** | **Integer** |  |  [optional]
**topSpace** | **Integer** |  |  [optional]
**bottomSpace** | **Integer** |  |  [optional]
**enableBuzzer** | **Boolean** |  |  [optional]
**invoicePrefix** | **String** |  |  [optional]
**printerNumber** | **Integer** |  |  [optional]
**_configuration** | **String** |  |  [optional]
**configurationPending** | **Boolean** |  |  [optional]
**configuredAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
