# CampaignitemSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**name** | **String** |  | 
**sku** | **String** |  | 
**points** | **Integer** |  | 
**campaignId** | **Integer** |  |  [optional]
