# PrintersApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**printersGet**](PrintersApi.md#printersGet) | **GET** /printers | Get All Printers
[**printersIdDelete**](PrintersApi.md#printersIdDelete) | **DELETE** /printers/{id} | Delete existing Printer
[**printersIdGet**](PrintersApi.md#printersIdGet) | **GET** /printers/{id} | Get existing Printer
[**printersIdPut**](PrintersApi.md#printersIdPut) | **PUT** /printers/{id} | Edit existing Printer
[**printersPost**](PrintersApi.md#printersPost) | **POST** /printers | Add new Printer

<a name="printersGet"></a>
# **printersGet**
> Printers printersGet(pagination, perPage, page)

Get All Printers

Returns a Json with data about all Printers of selected shop.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PrintersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

PrintersApi apiInstance = new PrintersApi();
Boolean pagination = true; // Boolean | Pagination parameter
Integer perPage = 56; // Integer | Results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    Printers result = apiInstance.printersGet(pagination, perPage, page);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PrintersApi#printersGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional]
 **perPage** | **Integer**| Results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

[**Printers**](Printers.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="printersIdDelete"></a>
# **printersIdDelete**
> printersIdDelete(id)

Delete existing Printer

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PrintersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

PrintersApi apiInstance = new PrintersApi();
String id = "id_example"; // String | id of the Printer that need to be deleted
try {
    apiInstance.printersIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling PrintersApi#printersIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Printer that need to be deleted |

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="printersIdGet"></a>
# **printersIdGet**
> Printers printersIdGet(id)

Get existing Printer

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PrintersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

PrintersApi apiInstance = new PrintersApi();
String id = "id_example"; // String | id of the Printer
try {
    Printers result = apiInstance.printersIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PrintersApi#printersIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Printer |

### Return type

[**Printers**](Printers.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="printersIdPut"></a>
# **printersIdPut**
> Printers printersIdPut(body, id)

Edit existing Printer

In this case you must specify the id in the URL and change the data you wanna update

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PrintersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

PrintersApi apiInstance = new PrintersApi();
Printers body = new Printers(); // Printers | Object data that need to be updated
String id = "id_example"; // String | id of the Printer that need to be updated
try {
    Printers result = apiInstance.printersIdPut(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PrintersApi#printersIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Printers**](Printers.md)| Object data that need to be updated |
 **id** | **String**| id of the Printer that need to be updated |

### Return type

[**Printers**](Printers.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="printersPost"></a>
# **printersPost**
> Printers printersPost(body)

Add new Printer

Returns a Json with the data of the new Printer

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PrintersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

PrintersApi apiInstance = new PrintersApi();
Printers body = new Printers(); // Printers | Printer object that needs to be added.
try {
    Printers result = apiInstance.printersPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PrintersApi#printersPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Printers**](Printers.md)| Printer object that needs to be added. |

### Return type

[**Printers**](Printers.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

