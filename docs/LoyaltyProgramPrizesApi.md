# LoyaltyProgramPrizesApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**prizesGet**](LoyaltyProgramPrizesApi.md#prizesGet) | **GET** /prizes | Get All Prizes
[**prizesIdDelete**](LoyaltyProgramPrizesApi.md#prizesIdDelete) | **DELETE** /prizes/{id} | Delete existing Prize
[**prizesIdGet**](LoyaltyProgramPrizesApi.md#prizesIdGet) | **GET** /prizes/{id} | Get existing Prize
[**prizesIdPut**](LoyaltyProgramPrizesApi.md#prizesIdPut) | **PUT** /prizes/{id} | Edit existing Prize
[**prizesPost**](LoyaltyProgramPrizesApi.md#prizesPost) | **POST** /prizes | Add new Prize

<a name="prizesGet"></a>
# **prizesGet**
> ChainsRules prizesGet(pagination, perPage, page)

Get All Prizes

Returns a Json with data about all prizes of selected shop.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LoyaltyProgramPrizesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

LoyaltyProgramPrizesApi apiInstance = new LoyaltyProgramPrizesApi();
Boolean pagination = true; // Boolean | Pagination parameter
Integer perPage = 56; // Integer | Results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    ChainsRules result = apiInstance.prizesGet(pagination, perPage, page);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LoyaltyProgramPrizesApi#prizesGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional]
 **perPage** | **Integer**| Results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

[**ChainsRules**](ChainsRules.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="prizesIdDelete"></a>
# **prizesIdDelete**
> prizesIdDelete(id)

Delete existing Prize

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LoyaltyProgramPrizesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

LoyaltyProgramPrizesApi apiInstance = new LoyaltyProgramPrizesApi();
String id = "id_example"; // String | id of the Prize that need to be deleted
try {
    apiInstance.prizesIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling LoyaltyProgramPrizesApi#prizesIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Prize that need to be deleted |

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="prizesIdGet"></a>
# **prizesIdGet**
> ChainsPrizes prizesIdGet(id)

Get existing Prize

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LoyaltyProgramPrizesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

LoyaltyProgramPrizesApi apiInstance = new LoyaltyProgramPrizesApi();
String id = "id_example"; // String | id of the Prize
try {
    ChainsPrizes result = apiInstance.prizesIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LoyaltyProgramPrizesApi#prizesIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Prize |

### Return type

[**ChainsPrizes**](ChainsPrizes.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="prizesIdPut"></a>
# **prizesIdPut**
> ChainsPrizes prizesIdPut(body, id)

Edit existing Prize

In this case you must specify the id in the URL and change the data you wanna update

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LoyaltyProgramPrizesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

LoyaltyProgramPrizesApi apiInstance = new LoyaltyProgramPrizesApi();
ChainsPrizes body = new ChainsPrizes(); // ChainsPrizes | Object data that need to be updated
String id = "id_example"; // String | id of the Prize that need to be updated
try {
    ChainsPrizes result = apiInstance.prizesIdPut(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LoyaltyProgramPrizesApi#prizesIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ChainsPrizes**](ChainsPrizes.md)| Object data that need to be updated |
 **id** | **String**| id of the Prize that need to be updated |

### Return type

[**ChainsPrizes**](ChainsPrizes.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="prizesPost"></a>
# **prizesPost**
> InlineResponse2012 prizesPost(body)

Add new Prize

Returns a Json with the data of the new Prize

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LoyaltyProgramPrizesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

LoyaltyProgramPrizesApi apiInstance = new LoyaltyProgramPrizesApi();
ChainsPrizes body = new ChainsPrizes(); // ChainsPrizes | Prize object that needs to be added.
try {
    InlineResponse2012 result = apiInstance.prizesPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LoyaltyProgramPrizesApi#prizesPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ChainsPrizes**](ChainsPrizes.md)| Prize object that needs to be added. |

### Return type

[**InlineResponse2012**](InlineResponse2012.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

