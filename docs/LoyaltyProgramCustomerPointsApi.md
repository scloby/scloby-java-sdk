# LoyaltyProgramCustomerPointsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**fidelitiesPointsIdGet**](LoyaltyProgramCustomerPointsApi.md#fidelitiesPointsIdGet) | **GET** /fidelities_points/{id} | Get existing Fidelity Point

<a name="fidelitiesPointsIdGet"></a>
# **fidelitiesPointsIdGet**
> InlineResponse2004 fidelitiesPointsIdGet(id)

Get existing Fidelity Point

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LoyaltyProgramCustomerPointsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

LoyaltyProgramCustomerPointsApi apiInstance = new LoyaltyProgramCustomerPointsApi();
String id = "id_example"; // String | id of the Fidelity Point
try {
    InlineResponse2004 result = apiInstance.fidelitiesPointsIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LoyaltyProgramCustomerPointsApi#fidelitiesPointsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Fidelity Point |

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

