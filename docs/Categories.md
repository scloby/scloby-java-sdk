# Categories

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**name** | **String** | Category name | 
**thumbnail** | **String** |  |  [optional]
**index** | **Integer** | Sorting index in visualization |  [optional]
**favorite** | **Boolean** |  |  [optional]
**display** | **Boolean** | Set to true if you want to show it in orders or in cash register |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**items** | [**List&lt;ItemSchema&gt;**](ItemSchema.md) |  |  [optional]
