# OrderitemingredientSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**orderItemId** | **Integer** |  |  [optional]
**name** | **String** |  | 
**type** | **String** | Type of ingredient variation, can be added or removed |  [optional]
**quantity** | **Integer** |  |  [optional]
**ingredientId** | **Integer** |  | 
**priceDifference** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
