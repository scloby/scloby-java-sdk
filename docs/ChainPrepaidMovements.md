# ChainPrepaidMovements

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**customerUuid** | **String** |  | 
**previousId** | **Integer** |  |  [optional]
**shopUuid** | **String** |  |  [optional]
**validFrom** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**validTo** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**credit** | [**BigDecimal**](BigDecimal.md) |  | 
**ticketCredit** | [**BigDecimal**](BigDecimal.md) |  | 
**movementTypeId** | **Integer** |  | 
**saleUuid** | **String** |  |  [optional]
**notes** | **String** |  |  [optional]
**createdbyId** | **Integer** |  |  [optional]
**amount** | **Object** |  |  [optional]
**ticketAmount** | **Object** |  |  [optional]
**movementType** | **Object** |  |  [optional]
