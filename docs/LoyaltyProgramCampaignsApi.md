# LoyaltyProgramCampaignsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**campaignsGet**](LoyaltyProgramCampaignsApi.md#campaignsGet) | **GET** /campaigns | Get All Campaigns
[**campaignsIdDelete**](LoyaltyProgramCampaignsApi.md#campaignsIdDelete) | **DELETE** /campaigns/{id} | Delete existing Campaign
[**campaignsIdGet**](LoyaltyProgramCampaignsApi.md#campaignsIdGet) | **GET** /campaigns/{id} | Get existing Campaign
[**campaignsIdPut**](LoyaltyProgramCampaignsApi.md#campaignsIdPut) | **PUT** /campaigns/{id} | Edit existing Campaign
[**campaignsPost**](LoyaltyProgramCampaignsApi.md#campaignsPost) | **POST** /campaigns | Add new Campaign

<a name="campaignsGet"></a>
# **campaignsGet**
> ChainsCampaigns campaignsGet(pagination, perPage, page)

Get All Campaigns

Returns a Json with data about all campaigns of selected shop.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LoyaltyProgramCampaignsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

LoyaltyProgramCampaignsApi apiInstance = new LoyaltyProgramCampaignsApi();
Boolean pagination = true; // Boolean | Pagination parameter
Integer perPage = 56; // Integer | Results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    ChainsCampaigns result = apiInstance.campaignsGet(pagination, perPage, page);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LoyaltyProgramCampaignsApi#campaignsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional]
 **perPage** | **Integer**| Results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

[**ChainsCampaigns**](ChainsCampaigns.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="campaignsIdDelete"></a>
# **campaignsIdDelete**
> campaignsIdDelete(id)

Delete existing Campaign

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LoyaltyProgramCampaignsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

LoyaltyProgramCampaignsApi apiInstance = new LoyaltyProgramCampaignsApi();
String id = "id_example"; // String | id of the Campaign that need to be deleted
try {
    apiInstance.campaignsIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling LoyaltyProgramCampaignsApi#campaignsIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Campaign that need to be deleted |

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="campaignsIdGet"></a>
# **campaignsIdGet**
> ChainsCampaigns campaignsIdGet(id)

Get existing Campaign

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LoyaltyProgramCampaignsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

LoyaltyProgramCampaignsApi apiInstance = new LoyaltyProgramCampaignsApi();
String id = "id_example"; // String | id of the Campaign
try {
    ChainsCampaigns result = apiInstance.campaignsIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LoyaltyProgramCampaignsApi#campaignsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Campaign |

### Return type

[**ChainsCampaigns**](ChainsCampaigns.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="campaignsIdPut"></a>
# **campaignsIdPut**
> ChainsCampaigns campaignsIdPut(body, id)

Edit existing Campaign

In this case you must specify the id in the URL and change the data you wanna update

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LoyaltyProgramCampaignsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

LoyaltyProgramCampaignsApi apiInstance = new LoyaltyProgramCampaignsApi();
ChainsCampaigns body = new ChainsCampaigns(); // ChainsCampaigns | Object data that need to be updated
String id = "id_example"; // String | id of the Campaign that need to be updated
try {
    ChainsCampaigns result = apiInstance.campaignsIdPut(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LoyaltyProgramCampaignsApi#campaignsIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ChainsCampaigns**](ChainsCampaigns.md)| Object data that need to be updated |
 **id** | **String**| id of the Campaign that need to be updated |

### Return type

[**ChainsCampaigns**](ChainsCampaigns.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="campaignsPost"></a>
# **campaignsPost**
> ChainsCampaigns campaignsPost(body)

Add new Campaign

Returns a Json with the data of the new Campaign

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LoyaltyProgramCampaignsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

LoyaltyProgramCampaignsApi apiInstance = new LoyaltyProgramCampaignsApi();
ChainsCampaigns body = new ChainsCampaigns(); // ChainsCampaigns | Campaign object that needs to be added.
try {
    ChainsCampaigns result = apiInstance.campaignsPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LoyaltyProgramCampaignsApi#campaignsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ChainsCampaigns**](ChainsCampaigns.md)| Campaign object that needs to be added. |

### Return type

[**ChainsCampaigns**](ChainsCampaigns.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

