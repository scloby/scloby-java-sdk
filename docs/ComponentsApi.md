# ComponentsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**componentsGet**](ComponentsApi.md#componentsGet) | **GET** /components | Get All Components
[**componentsIdDelete**](ComponentsApi.md#componentsIdDelete) | **DELETE** /components/{id} | Delete existing Component
[**componentsIdGet**](ComponentsApi.md#componentsIdGet) | **GET** /components/{id} | Get existing component
[**componentsIdPut**](ComponentsApi.md#componentsIdPut) | **PUT** /components/{id} | Edit existing Component
[**componentsPost**](ComponentsApi.md#componentsPost) | **POST** /components | Add new Component

<a name="componentsGet"></a>
# **componentsGet**
> Components componentsGet(pagination, perPage, page)

Get All Components

Returns a Json with data about all components (ingredients) of selected shop.  Paginated by default (per_page &#x3D; 1000)

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ComponentsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

ComponentsApi apiInstance = new ComponentsApi();
Boolean pagination = true; // Boolean | Pagination parameter
Integer perPage = 56; // Integer | Results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    Components result = apiInstance.componentsGet(pagination, perPage, page);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ComponentsApi#componentsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional]
 **perPage** | **Integer**| Results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

[**Components**](Components.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="componentsIdDelete"></a>
# **componentsIdDelete**
> componentsIdDelete(id)

Delete existing Component

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ComponentsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

ComponentsApi apiInstance = new ComponentsApi();
String id = "id_example"; // String | id of the Component that need to be deleted
try {
    apiInstance.componentsIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling ComponentsApi#componentsIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Component that need to be deleted |

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="componentsIdGet"></a>
# **componentsIdGet**
> Components componentsIdGet(id)

Get existing component

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ComponentsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

ComponentsApi apiInstance = new ComponentsApi();
String id = "id_example"; // String | id of the component
try {
    Components result = apiInstance.componentsIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ComponentsApi#componentsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the component |

### Return type

[**Components**](Components.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="componentsIdPut"></a>
# **componentsIdPut**
> Components componentsIdPut(body, id)

Edit existing Component

In this case you must specify the id in the URL and change the data you wanna update

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ComponentsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

ComponentsApi apiInstance = new ComponentsApi();
Components body = new Components(); // Components | Object data that need to be updated
String id = "id_example"; // String | id of the component that need to be updated
try {
    Components result = apiInstance.componentsIdPut(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ComponentsApi#componentsIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Components**](Components.md)| Object data that need to be updated |
 **id** | **String**| id of the component that need to be updated |

### Return type

[**Components**](Components.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="componentsPost"></a>
# **componentsPost**
> Components componentsPost(body)

Add new Component

Returns a Json with the data of the new Component.  You can set a price difference if you want that when you add this component to an item in a order, the final price will be increased of a particular amount.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ComponentsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

ComponentsApi apiInstance = new ComponentsApi();
Components body = new Components(); // Components | Component object that needs to be added.
try {
    Components result = apiInstance.componentsPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ComponentsApi#componentsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Components**](Components.md)| Component object that needs to be added. |

### Return type

[**Components**](Components.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

