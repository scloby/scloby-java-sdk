# BookingtagSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**bookingId** | **Integer** |  |  [optional]
**tag** | **String** |  |  [optional]
