# PricechangeSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**index** | **Integer** |  | 
**type** | **String** | Type of price_change, can be discount_fix, discount_perc, surcharge_fix, surcharge_perc | 
**value** | [**BigDecimal**](BigDecimal.md) | Value of discount/surcharge, for percentage values use 10.50 for 10.5% | 
**description** | **String** | Discount/surcharge description | 
**saleId** | **Integer** |  |  [optional]
**saleItemId** | **Integer** |  |  [optional]
**ruleId** | **Integer** |  |  [optional]
**ruleName** | **String** |  |  [optional]
**prizeId** | **Integer** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
