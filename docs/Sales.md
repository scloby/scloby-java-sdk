# Sales

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**uuid** | **String** |  |  [optional]
**name** | **String** |  | 
**externalId** | **String** |  |  [optional]
**channel** | **String** |  |  [optional]
**saleNumber** | **Integer** |  |  [optional]
**isSummary** | **Boolean** |  |  [optional]
**notes** | **String** |  |  [optional]
**saleParentId** | **Integer** |  |  [optional]
**saleParentUuid** | **String** |  |  [optional]
**orderId** | **Integer** |  |  [optional]
**orderUuid** | **String** |  |  [optional]
**sclobyShopId** | **String** | Scloby Pass Shop ID |  [optional]
**assignedId** | **Integer** |  |  [optional]
**assignedName** | **String** |  |  [optional]
**sellerId** | **Integer** |  | 
**sellerName** | **String** |  | 
**customerTaxCode** | **String** |  |  [optional]
**openAt** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**closedAt** | [**OffsetDateTime**](OffsetDateTime.md) | Closing (or Storing) date |  [optional]
**lastupdateAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**lastupdateBy** | **Integer** |  |  [optional]
**status** | **String** | Sale status, can be open, closed or stored | 
**amount** | [**BigDecimal**](BigDecimal.md) | Sum of price * quantity + discount/surcharges of all sale_items |  [optional]
**change** | [**BigDecimal**](BigDecimal.md) | Payment change (also known as &#x27;resto&#x27;) |  [optional]
**changeType** | **String** | Change type, can be &#x27;cash&#x27;, &#x27;ticket&#x27; or &#x27;other&#x27; |  [optional]
**finalAmount** | [**BigDecimal**](BigDecimal.md) | Sum of price * quantity + discount/surcharges of all sale_items + discount/surcharges on amount |  [optional]
**finalNetAmount** | [**BigDecimal**](BigDecimal.md) | Sum of final_price * quantity of all sale_items |  [optional]
**currency** | **String** |  | 
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**createdbyId** | **Integer** |  |  [optional]
**updatedbyId** | **Integer** |  |  [optional]
**deletedbyId** | **Integer** |  |  [optional]
**priceChanges** | [**List&lt;PricechangeSchema&gt;**](PricechangeSchema.md) |  |  [optional]
**saleItems** | [**List&lt;SaleitemSchema&gt;**](SaleitemSchema.md) |  |  [optional]
**saleCustomer** | [**SalecustomerSchema**](SalecustomerSchema.md) |  |  [optional]
**payments** | [**List&lt;SalepaymentSchema&gt;**](SalepaymentSchema.md) |  |  [optional]
**saleDocuments** | [**List&lt;SaledocumentSchema&gt;**](SaledocumentSchema.md) |  |  [optional]
**eInvoice** | [**SaleeinvoiceSchema**](SaleeinvoiceSchema.md) |  |  [optional]
