# SessionApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sessionsMeGet**](SessionApi.md#sessionsMeGet) | **GET** /sessions/me | Get Session

<a name="sessionsMeGet"></a>
# **sessionsMeGet**
> InlineResponse2003 sessionsMeGet()

Get Session

Get the active session

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SessionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

SessionApi apiInstance = new SessionApi();
try {
    InlineResponse2003 result = apiInstance.sessionsMeGet();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SessionApi#sessionsMeGet");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

