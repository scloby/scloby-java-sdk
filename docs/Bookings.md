# Bookings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**shiftId** | **Integer** |  |  [optional]
**customerId** | **Integer** |  | 
**source** | **String** |  |  [optional]
**bookedFor** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**duration** | **Integer** |  | 
**status** | **String** |  | 
**people** | **Integer** |  |  [optional]
**notes** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deletedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**createdbyId** | **Integer** |  |  [optional]
**updatedbyId** | **Integer** |  |  [optional]
**deletedbyId** | **Integer** |  |  [optional]
**tags** | [**List&lt;BookingtagSchema&gt;**](BookingtagSchema.md) |  |  [optional]
**tables** | [**List&lt;BookingtableSchema&gt;**](BookingtableSchema.md) |  |  [optional]
**customer** | [**CustomerSchema**](CustomerSchema.md) |  |  [optional]
