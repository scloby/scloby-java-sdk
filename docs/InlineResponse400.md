# InlineResponse400

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** |  |  [optional]
**error** | [**InlineResponse400Error**](InlineResponse400Error.md) |  |  [optional]
