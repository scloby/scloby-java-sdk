# RoomsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**roomsGet**](RoomsApi.md#roomsGet) | **GET** /rooms | Get All Rooms
[**roomsIdDelete**](RoomsApi.md#roomsIdDelete) | **DELETE** /rooms/{id} | Delete existing Room
[**roomsIdGet**](RoomsApi.md#roomsIdGet) | **GET** /rooms/{id} | Get existing Printer
[**roomsIdPut**](RoomsApi.md#roomsIdPut) | **PUT** /rooms/{id} | Edit existing Room
[**roomsPost**](RoomsApi.md#roomsPost) | **POST** /rooms | Add new Room

<a name="roomsGet"></a>
# **roomsGet**
> Rooms roomsGet(pagination, perPage, page)

Get All Rooms

Returns a Json with data about all rooms of selected shop.  Paginated by default (per_page&#x3D;1000)

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RoomsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

RoomsApi apiInstance = new RoomsApi();
Boolean pagination = true; // Boolean | Pagination parameter
Integer perPage = 56; // Integer | Results_per_page
Integer page = 56; // Integer | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
try {
    Rooms result = apiInstance.roomsGet(pagination, perPage, page);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoomsApi#roomsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional]
 **perPage** | **Integer**| Results_per_page | [optional]
 **page** | **Integer**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional]

### Return type

[**Rooms**](Rooms.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="roomsIdDelete"></a>
# **roomsIdDelete**
> roomsIdDelete(id)

Delete existing Room

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RoomsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

RoomsApi apiInstance = new RoomsApi();
String id = "id_example"; // String | id of the Room that need to be deleted
try {
    apiInstance.roomsIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling RoomsApi#roomsIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Room that need to be deleted |

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="roomsIdGet"></a>
# **roomsIdGet**
> Rooms roomsIdGet(id)

Get existing Printer

In this case you must specify the id in the URL

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RoomsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

RoomsApi apiInstance = new RoomsApi();
String id = "id_example"; // String | id of the Printer
try {
    Rooms result = apiInstance.roomsIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoomsApi#roomsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Printer |

### Return type

[**Rooms**](Rooms.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="roomsIdPut"></a>
# **roomsIdPut**
> Rooms roomsIdPut(body, id)

Edit existing Room

In this case you must specify the id in the URL and change the data you wanna update

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RoomsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

RoomsApi apiInstance = new RoomsApi();
Rooms body = new Rooms(); // Rooms | Object data that need to be updated
String id = "id_example"; // String | id of the Room that need to be updated
try {
    Rooms result = apiInstance.roomsIdPut(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoomsApi#roomsIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Rooms**](Rooms.md)| Object data that need to be updated |
 **id** | **String**| id of the Room that need to be updated |

### Return type

[**Rooms**](Rooms.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="roomsPost"></a>
# **roomsPost**
> Rooms roomsPost(body)

Add new Room

Returns a Json with the data of the new Room

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RoomsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oAuth2AuthCode
OAuth oAuth2AuthCode = (OAuth) defaultClient.getAuthentication("oAuth2AuthCode");
oAuth2AuthCode.setAccessToken("YOUR ACCESS TOKEN");

RoomsApi apiInstance = new RoomsApi();
Rooms body = new Rooms(); // Rooms | Room object that needs to be added.
try {
    Rooms result = apiInstance.roomsPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RoomsApi#roomsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Rooms**](Rooms.md)| Room object that needs to be added. |

### Return type

[**Rooms**](Rooms.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

